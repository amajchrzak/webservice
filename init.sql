GRANT CONNECT ON DATABASE "user" TO "user";
GRANT ALL PRIVILEGES ON DATABASE "user" TO "user";
GRANT ALL ON ALL TABLES IN SCHEMA public TO "user";
GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO "user";
GRANT ALL ON ALL FUNCTIONS IN SCHEMA public TO "user";