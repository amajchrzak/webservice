package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func AddTask(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		var formattedBody inferfaces.UserTasks
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Wprowdzono błędne dane!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Coś poszło nie tak!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		userId := mux.Vars(r)
		id, err := strconv.Atoi(userId["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się pobrać id użytkownika!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		status := model.InsertTask(id, formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}

func DeleteTask(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		userId := mux.Vars(r)
		id, err := strconv.Atoi(userId["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się pobrać id użytkownika!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		status := model.DeleteTask(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}
