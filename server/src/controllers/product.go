package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/services/productService"
)

func AddProduct(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Niepoprawnie wypełniłeś formularz!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		var formattedBody inferfaces.ProductDatabaseRepresentation
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Niepoprawnie wypełniłeś formularz!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		product, statuts := productService.AddProduct(formattedBody)
		if statuts != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"id":          product.ID,
			"deliverId":   product.DeliverID,
			"name":        product.Name,
			"description": product.Description,
			"price":       product.Price,
			"message":     inferfaces.MESSAGE,
		})
	}
}

func GetProduct(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		product := productService.GetProductByID(id)

		json.NewEncoder(w).Encode(product)
	}
}

func GetAllProducts(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		products, status := productService.GetProducts()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"products": products,
			"message":  inferfaces.MESSAGE,
		})
	}
}

func DeleteProduct(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		message := productService.DeleteProductByID(id)

		json.NewEncoder(w).Encode(message)
	}
}

func UpdateProduct(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.ProductDatabaseRepresentation
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		updatedProduct := productService.UpdateProductByID(id, formattedBody)

		json.NewEncoder(w).Encode(updatedProduct)
	}
}

func GetProductByDeliverID(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		products, status := productService.GetProductsFromSpecifiedDeliver(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message":  inferfaces.MESSAGE,
			"products": products,
		})
	}
}
