package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func UpdateRole(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		userId := mux.Vars(r)
		id, err := strconv.Atoi(userId["userId"])

		if err != nil {
			inferfaces.MESSAGE = "Nie udało się przeprowadzić konwersji!"
			w.WriteHeader(http.StatusInternalServerError)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			inferfaces.MESSAGE = "Źle wypełniono formularz!"
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		var role inferfaces.Role
		err = json.Unmarshal(body, &role)
		if err != nil {
			inferfaces.MESSAGE = "Coś poszło nie tak z ustawieniem nowej roli!"
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		//tutaj jest błąd
		status := model.UpdateUserRole(id, role.Role)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		// if doesn't exist user in questionnaire table add him
		if role.Role == 1 {
			status = model.InsertIntoQuestionnaireTable(id)
			if status != http.StatusOK {
				w.WriteHeader(http.StatusBadRequest)

				json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
				return
			}
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}
