package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func UpdateQuestionnaire(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		var formattedBody inferfaces.QuestionnaireFormUpdate

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Formularz został niepoprawnie wypełniony!"

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		id := helpers.GetLoggedInUserID(r)
		err = json.Unmarshal(body, &formattedBody)
		fmt.Println(err)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Coś poszło nie tak!"

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		status := model.UpdateQuestionnaire(formattedBody, id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się zaktualizować danych!"

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
	}
}

func GetProfileDetails(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		id := helpers.GetLoggedInUserID(r)
		profile, status := model.GetProfileDetails(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		tasks, status := model.GetTasks(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"profile": profile,
			"tasks":   tasks,
			"message": inferfaces.MESSAGE,
		})
	}
}
