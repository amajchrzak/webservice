package controllers

import (
	"bytes"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model/fakturomaniaModel"
	"gitlab.com/amajchrzak/webservice/server/src/services/fakturomaniaService"
	fakturomaniabuilder "gitlab.com/amajchrzak/webservice/server/src/services/fakturomaniaService/fakturomaniaBuilder"
)

func createSessionFakturomania() (string, int) {
	client := &http.Client{}

	loginFakturomaniaData := map[string]interface{}{
		"email":    inferfaces.LOGIN_FAKTUROMANIA,
		"password": inferfaces.PASSWORD_FAKTUROMANIA,
		"remember": true,
	}
	convertLoginFakturomaniaToJSON, err := json.Marshal(loginFakturomaniaData)
	if err != nil {
		inferfaces.MESSAGE = "Przekazano niepoprawny format danych!"

		return "", http.StatusBadRequest
	}

	req, err := http.NewRequest("POST", inferfaces.CREATE_SESSION_ENDPOINT, bytes.NewBuffer(convertLoginFakturomaniaToJSON))
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się utworzyć sesji w portalu fakturomania!"

		return "", http.StatusBadRequest
	}
	req.Header.Set("Content-Type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się zalogować do portalu fakturomania!"

		return "", http.StatusBadRequest
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		inferfaces.MESSAGE = "Przekazano błędne dane!"

		return "", http.StatusBadRequest
	}

	var formattedBody inferfaces.SessionFakturomaniaRepresentation
	err = json.Unmarshal(body, &formattedBody)
	if err != nil {
		inferfaces.MESSAGE = "Coś poszło nie tak!"

		return "", http.StatusBadRequest
	}

	return formattedBody.Value, http.StatusOK
}

func CreateFakturomaniaContractor(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		authToken, status := createSessionFakturomania()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Przekazano błędne dane!"

			return
		}

		client := &http.Client{}
		req, err := http.NewRequest("POST", inferfaces.CREATE_FAKTUROMANIA_CONTRACTOR, bytes.NewBuffer(body))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się utworzyć kontraktora!"

			return
		}

		req.Header.Set("Auth-Token", authToken)
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się wysłać zapytania z portalu fakturomania!"

			return
		}
		defer resp.Body.Close()

		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Przekazano błędne dane!"

			return
		}

		var formattedBody inferfaces.FakturomaniaContractorResponse
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Coś poszło nie tak!"

		}

		idLoggedInUser := helpers.GetLoggedInUserID(r)
		fakturomaniaContractorDB := setFakturomaniaContractorDB(formattedBody)
		fakturomaniaService.AddFakturomaniaContractor(&fakturomaniaContractorDB, idLoggedInUser)

		json.NewEncoder(w).Encode(map[string]interface{}{
			"ContractorId":        formattedBody.ContractorId,
			"ContractorVersionId": formattedBody.ContractorVersionId,
			"Name":                formattedBody.Name,
			"NipPrefix":           formattedBody.NipPrefix,
			"Nip":                 formattedBody.Nip,
			"Street":              formattedBody.Street,
			"PostalCode":          formattedBody.PostalCode,
			"PostalCity":          formattedBody.PostalCity,
			"CustomerAccountId":   formattedBody.CustomerAccountId,
			"SupplierAccountId":   formattedBody.SupplierAccountId,
			"message":             inferfaces.MESSAGE,
		})
	}
}

func setFakturomaniaContractorDB(formattedBody inferfaces.FakturomaniaContractorResponse) inferfaces.FakturomaniaContractorRepresentation {
	var fakturomaniaContractorDB inferfaces.FakturomaniaContractorRepresentation
	fakturomaniaContractorDB.ContractorId = formattedBody.ContractorId
	fakturomaniaContractorDB.ContractorVersionId = formattedBody.ContractorVersionId

	return fakturomaniaContractorDB
}

func GetFakturomaniaContractorByUserId(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		pathID := mux.Vars(r)
		_, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		client := &http.Client{}

		authToken, status := createSessionFakturomania()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		getContactorEndpointByID := inferfaces.GET_FAKTUROMANIA_CONTRACTOR_BY_ID + pathID["id"]
		req, err := http.NewRequest("GET", getContactorEndpointByID, nil)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		req.Header.Set("Auth-Token", authToken)
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		defer resp.Body.Close()

		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody inferfaces.FakturomaniaContractorResponse
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(formattedBody)
	}
}

func CreateInvoice(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		authToken, status := createSessionFakturomania()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		body, status = fakturomaniabuilder.Build(body)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		client := &http.Client{}
		fmt.Println(bytes.NewBuffer(body))
		req, err := http.NewRequest("POST", inferfaces.CREATE_FAKTUROMANIA_INVOICE, bytes.NewBuffer(body))
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		req.Header.Set("Auth-Token", authToken)
		req.Header.Set("Content-Type", "application/json")

		resp, err := client.Do(req)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		fmt.Println(resp)
		body, err = ioutil.ReadAll(resp.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		var formattedBody inferfaces.Invoices
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		fmt.Println(formattedBody.InvoiceDetails.InvoiceID)
		fmt.Println(formattedBody.InvoiceDetails.Created)
		fmt.Println(formattedBody.InvoiceInfo.DocumentName)
		fmt.Println(formattedBody.ContractorInfo.ContractorId)
		fakturomaniaContractorIDRepresentationDB, status := fakturomaniaModel.FindFakturomaniaContractorByID(formattedBody.ContractorInfo.ContractorId)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się utworzyć faktury!"

			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": inferfaces.MESSAGE,
			})
			return
		}

		status = fakturomaniaModel.InsertFakturomaniaInvoice(fakturomaniaContractorIDRepresentationDB, formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się utworzyć faktury!"

			json.NewEncoder(w).Encode(map[string]interface{}{
				"message": inferfaces.MESSAGE,
			})
			return
		}

		defer resp.Body.Close()

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}

func GetFakturomaniaContractors(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		fakturomaniaContractors, status := fakturomaniaModel.GetFakturomaniaContractors()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się pobrać listy kontraktorów."

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"fakturomaniaContractors": fakturomaniaContractors,
			"message":                 inferfaces.MESSAGE,
		})
	}
}

func GetFakturomaniaInvoice(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		// authToken, status := createSessionFakturomania()
		// fmt.Println(authToken)
		// os.Exit(1)

		// if status != http.StatusOK {
		// 	w.WriteHeader(http.StatusBadRequest)

		// 	return
		// }

		// req, err := http.NewRequest("GET", inferfaces.CREATE_FAKTUROMANIA_INVOICE, nil)
		// if err != nil {
		// 	w.WriteHeader(http.StatusBadRequest)

		// 	return
		// }
		// req.Header.Set("Auth-Token", authToken)
		// req.Header.Set("Content-Type", "application/json")
		// client := &http.Client{}
		// resp, err := client.Do(req)
		// if err != nil {
		// 	w.WriteHeader(http.StatusBadRequest)

		// 	return
		// }
		// fmt.Println(resp)
		// defer resp.Body.Close()

		// body, err := ioutil.ReadAll(resp.Body)
		// if err != nil {
		// 	w.WriteHeader(http.StatusBadRequest)

		// 	return
		// }

		// var formattedBody inferfaces.FakturomaniaContractorResponse
		// err = json.Unmarshal(body, &formattedBody)
		// if err != nil {
		// 	w.WriteHeader(http.StatusBadRequest)

		// 	return
		// }

		// json.NewEncoder(w).Encode(map[string]interface{}{
		// 	"message": inferfaces.MESSAGE,
		// })
	}
}

func GetInvoiceById(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		requestedInvoiceId := mux.Vars(r)

		authToken, status := createSessionFakturomania()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		getInvoiceByIdUrl := inferfaces.GET_INVOICE_BY_ID + requestedInvoiceId["invoiceId"] + "/pdf"
		req, err := http.NewRequest("GET", getInvoiceByIdUrl, nil)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Faktura nie istnieje!"

			return
		}
		req.Header.Set("Auth-Token", authToken)
		req.Header.Set("Content-Type", "application/json")
		client := &http.Client{}

		resp, err := client.Do(req)
		fmt.Println(err)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się pobrać faktury!"

			return
		}
		body, err := ioutil.ReadAll(resp.Body)
		// d1 := []byte(body)
		// err = ioutil.WriteFile("/home/dell/testteeee", d1, 0644)

		//czyli tak save to tmp folder konwersja do base64
		//wteedy należy to odczytać z base64 jako pdf
		//może trzeba pokombinować z headerami mime type etc..
		invoiceEncodedToBase64 := base64.StdEncoding.EncodeToString(body)
		// d64, err := base64.StdEncoding.DecodeString(b64)
		// if err != nil {
		// 	panic(err)
		// }

		// fmt.Println(string(d64))
		// fmt.Println(b64)

		json.NewEncoder(w).Encode(map[string]interface{}{
			"file":    invoiceEncodedToBase64,
			"message": inferfaces.MESSAGE,
		})
	}
}

func GetInvoices(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		invoices, status := fakturomaniaModel.GetInvoices()
		if status != http.StatusOK {
			inferfaces.MESSAGE = "Nie udało się pobrać faktur!"
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		fmt.Println(invoices)

		// json.NewEncoder(w).Encode(map[string]interface{}{
		// 	"invoices": invoices,
		// 	"message":  inferfaces.MESSAGE,
		// })

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message":  inferfaces.MESSAGE,
			"invoices": invoices,
		})
	}
}
