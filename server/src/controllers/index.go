package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/helpers"
)

func Index(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": "SUCCESS-TEST-TEST",
		})
	}
}
