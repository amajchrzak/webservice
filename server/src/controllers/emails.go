package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/services/emailService"
)

func SendMail(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Lista mailingowa jest nieprawidłowa!"

			return
		}

		var formattedBody inferfaces.SendEmail
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Coś poszło nie tak!"

			return
		}

		status := emailService.SendGroupEmail(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się wysłać maila!"

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}
