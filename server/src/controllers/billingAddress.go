package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
	"gitlab.com/amajchrzak/webservice/server/src/services/billingAddressService"
)

func CreateBillingAddress(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.BillingAddressDatabaseRepresentation
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		formattedBody.UserID = helpers.GetLoggedInUserID(r)
		billingAddress := billingAddressService.AddBillingAddress(formattedBody)

		json.NewEncoder(w).Encode(billingAddress)
	}
}

func UpdateBillingAddress(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.BillingAddressDatabaseRepresentation
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		formattedBody.UserID = helpers.GetLoggedInUserID(r)
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		billingAddressService.CheckRightUser(id, formattedBody.UserID, w)
		billingAddress := model.FindBillingAddressByID(id)
		updatedBillingAddress := billingAddressService.ChangeBillingAddressData(&billingAddress, &formattedBody)

		json.NewEncoder(w).Encode(updatedBillingAddress)
	}
}

func GetBillingAddresses(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		billingAddresses, status := model.GetBillingAddresses()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message":        inferfaces.MESSAGE,
			"billingAddress": billingAddresses,
		})
	}
}
