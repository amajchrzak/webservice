package user

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
	"gitlab.com/amajchrzak/webservice/server/src/services/emailService"
	"gitlab.com/amajchrzak/webservice/server/src/services/userService"
)

// type BodyStruct struct {
// 	FormattedBody inferfaces
// }

func Register(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			inferfaces.MESSAGE = "Formularz wypełniony niepoprawnie!"
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		var formattedBody inferfaces.Register
		// err := json.NewDecoder(r.Body).Decode(&formattedBody)
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			inferfaces.MESSAGE = "Coś poszło nie tak!"
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		user, status := userService.PrepareDataToRegister(&formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		newUserCredentials, status := model.FindOneBy(user.Email)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}
		user.ID = newUserCredentials.ID
		//insert into billingAddress
		status = model.InsertBillingAddressRegister(user)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}
		// status = model.InsertIntoBillingAddress(user)
		// if status != http.StatusOK {
		// 	w.WriteHeader(http.StatusBadRequest)

		// 	json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
		// 	return
		// }

		json.NewEncoder(w).Encode(map[string]interface{}{
			"id":      user.ID,
			"email":   user.Email,
			"role":    user.Role,
			"message": inferfaces.MESSAGE,
		})
	}
}

func Login(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": "Wprowadzono niepoprawne dane"})
			return
		}

		var formattedBody inferfaces.Login
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": "Wprowadzono niepoprawne dane"})
			return
		}

		user, token, status := userService.PrepareDataToLogin(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		userService.SetCookieToken(w, token, time.Now().Add(time.Hour*24), "jwt")

		json.NewEncoder(w).Encode(map[string]interface{}{
			"id":      user.ID,
			"email":   user.Email,
			"role":    user.Role,
			"jwt":     token,
			"message": inferfaces.MESSAGE,
		})
	}
}

func Logout(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		userService.SetCookieToken(w, "", time.Now().Add(-time.Hour), "jwt")

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": "SUCCESS",
		})
	}
}

func SendResetPasswordProcedure(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Wprowadź email!"

			return
		}

		var formattedBody inferfaces.Email
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Coś poszło nie tak!"

			return
		}

		status := emailService.StartResetPasswordProcedureEmail(formattedBody.Email)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}

func ResetPassword(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.ResetPassword
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := userService.CheckRequestCorrectness(
			formattedBody.NewPassword,
			formattedBody.RetypedNewPassword,
			formattedBody.Token)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}

func GetUsers(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		users, status := model.GetUsers()
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Nie udało się wyświetlić użytkowników!"

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"users":   users,
			"message": inferfaces.MESSAGE,
		})
	}
}

func ChangePassword(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)

	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Formularz wypełniono niepoprawnie!"

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		var formattedBody inferfaces.ChangePassword
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)
			inferfaces.MESSAGE = "Cos poszlo nie tak!"

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		userId := mux.Vars(r)
		id, err := strconv.Atoi(userId["id"])
		if err != nil {
			inferfaces.MESSAGE = "Nie udało się przeprowadzić konwersji!"
			w.WriteHeader(http.StatusInternalServerError)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		user, status := model.FindOneByID(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		status = userService.ChangePasswordValidation(
			user.Password,
			formattedBody.Password,
			formattedBody.NewPassword,
			formattedBody.RetypedNewPassword,
			id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}
		//wyjmuje hasło, przekazuje hash i oldPassword
		//przekazuje nowe hasła, porównuje hashuje
		//zmieniam w bazie danych ...
		//todo change password.
		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}

func GetSpecificUserDetails(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		userId := mux.Vars(r)
		id, err := strconv.Atoi(userId["userId"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		profile, status := model.GetProfileDetails(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		tasks, status := model.GetTasks(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"profile": profile,
			"tasks":   tasks,
			"message": inferfaces.MESSAGE,
		})
	}
}
