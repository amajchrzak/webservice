package user_test

import (
	"database/sql"
	"log"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"

	_ "github.com/lib/pq"
	"gitlab.com/amajchrzak/webservice/server/src/controllers/user"
)

const (
	dbDriver = "postgres"
	dbSource = "postgresql://root:mysecretpassword1.@localhost/serp?sslmode=disable"
)

var DB *sql.DB

func TestMain(m *testing.M) {
	_, err := sql.Open(dbDriver, dbSource)
	if err != nil {
		log.Fatal("cannot connect to db:", err)
	}

	os.Exit(m.Run())
}

func TestRegister(t *testing.T) {
	// var m *testing.M

	// _, err := sql.Open(dbDriver, dbSource)
	// if err != nil {
	// 	log.Fatal("cannot connect to db:", err)
	// }

	// os.Exit(m.Run())
	//1. stworzyć strukturę !!!

	tt := []struct {
		name       string
		method     string
		body       string
		want       string
		statusCode int
	}{
		{
			name:   "right register data",
			method: http.MethodPost,
			body: `{
				"email":           "adam334@adam.adam",
				"password":        "b1234567890#",
				"retypedPassword": "b1234567890#"
			}`,
			want: `{
				"email": "adam334@adam.adam", 
				"id": 131, 
				"message": "SUCCESS",
				"role": 0
			}`,
			statusCode: http.StatusOK,
		},
	}

	for _, tc := range tt {
		t.Run(tc.name, func(t *testing.T) {
			// router := mux.NewRouter()

			request := httptest.NewRequest(tc.method, "/register", strings.NewReader(tc.body))
			responseRecorder := httptest.NewRecorder()

			user.Register(responseRecorder, request)
			// http.DefaultServeMux.ServeHTTP(responseRecorder, request)

			// fmt.Println(responseRecorder.Body.String())

			if responseRecorder.Code != tc.statusCode {
				t.Errorf("Want status '%d', got '%d'", tc.statusCode, responseRecorder.Code)
			}
		})
	}
}
