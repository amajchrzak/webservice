package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func InsertUserToWorkSchedule(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.Schedule
		err = json.Unmarshal(body, &formattedBody)
		fmt.Println(err)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		status := model.InsertUserToWorkSchedule(formattedBody)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusBadRequest)

			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})
			return
		}

		// formattedBody.UserID = helpers.GetLoggedInUserID(r)
		// _ = calendarService.CreateNewEvent(formattedBody)

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message": inferfaces.MESSAGE,
		})
	}
}

func GetEmployeesSchedule(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		employeesSchedule, status := model.GetEmployeesSchedule()
		if status != http.StatusOK {
			inferfaces.MESSAGE = "Nie udało się pobrać danych na temat grafiku z bazy danych"
			w.WriteHeader(http.StatusBadRequest)
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}
		json.NewEncoder(w).Encode(map[string]interface{}{
			"employeesScheduleData": employeesSchedule,
			"message":               inferfaces.MESSAGE,
		})
	}
}
