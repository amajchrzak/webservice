package controllers

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/services/deliverService"
)

func AddDeliver(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.DeliverDatabaseRepresentation
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		deliver := deliverService.AddDeliver(formattedBody)

		json.NewEncoder(w).Encode(deliver)
	}
}

func DeleteDeliver(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		message := deliverService.DeleteDeliverByID(id)

		json.NewEncoder(w).Encode(message)
	}
}

func GetDeliver(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		deliver := deliverService.GetDeliverByID(id)

		json.NewEncoder(w).Encode(deliver)
	}
}

func GetAllDelivers(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		delivers, status := deliverService.GetDelivers()
		if status != http.StatusOK {
			w.WriteHeader(status)
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		json.NewEncoder(w).Encode(map[string]interface{}{
			"message":  inferfaces.MESSAGE,
			"delivers": delivers,
		})
	}
}

func UpdateDeliver(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method == "OPTIONS" {

	} else {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		var formattedBody inferfaces.DeliverDatabaseRepresentation
		err = json.Unmarshal(body, &formattedBody)
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}
		pathID := mux.Vars(r)
		id, err := strconv.Atoi(pathID["id"])
		if err != nil {
			w.WriteHeader(http.StatusBadRequest)

			return
		}

		updatedDeliver := deliverService.UpdateDeliverByID(id, formattedBody)

		json.NewEncoder(w).Encode(updatedDeliver)
	}
}
