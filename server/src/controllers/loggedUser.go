package controllers

import (
	"encoding/json"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func GetLoggedUserDetails(w http.ResponseWriter, r *http.Request) {
	helpers.SetHeaders(w, r)
	if r.Method != "OPTIONS" {
		id := helpers.GetLoggedInUserID(r)
		if id == 0 {
			w.WriteHeader(http.StatusUnauthorized)
			inferfaces.MESSAGE = "Użytkonik nie jest zalogowany!"
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}
		user, status := model.FindOneById(id)
		if status != http.StatusOK {
			w.WriteHeader(http.StatusUnauthorized)
			json.NewEncoder(w).Encode(map[string]interface{}{"message": inferfaces.MESSAGE})

			return
		}

		var userReturn inferfaces.UserDetailsV2ToChange
		userReturn.Id = user.Id
		userReturn.FakturomaniaContractorId = int(user.FakturomaniaContractorId.Int32)
		userReturn.Role = user.Role
		userReturn.Email = user.Email

		json.NewEncoder(w).Encode(map[string]interface{}{
			"user":    userReturn,
			"message": inferfaces.MESSAGE,
		})
	}
}
