package inferfaces

import (
	"database/sql"
	"time"
)

type UserDatabaseRepresentation struct {
	ID                       int
	Email                    string
	Password                 string
	Role                     int
	UUID                     sql.NullString
	FakturomaniaContractorID sql.NullInt32
}

type Register struct {
	ID              int    `json:"id"`
	Email           string `json:"email"`
	Password        string `json:"password"`
	RetypedPassword string `json:"retypedPassword"`
	Role            int    `json:"role"`
	Name            string `json:"name"`
	Surname         string `json:"surname"`
}

type ResponseUser struct {
	ID    int
	Email string
	Role  int
}

type Login struct {
	ID       int
	Email    string
	Password string
	Role     int
}

type ResponseLogin struct {
	ID    int
	Email string
}

type Email struct {
	Email string
}

type ResetPassword struct {
	NewPassword        string
	RetypedNewPassword string
	Token              string
}

type BillingAddressDatabaseRepresentation struct {
	ID          int
	UserID      int
	Name        string
	Surname     string
	Address     string
	PostalCode  string
	PostalCity  string
	PhoneNumber string
	Country     string
}

type ProductDatabaseRepresentation struct {
	ID          int    `json:"id"`
	DeliverID   int    `json:"deliverId"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Price       string `json:"price"`
}

type DeliverDatabaseRepresentation struct {
	ID          int    `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	City        string `json:"city"`
	PostalCode  string `json:"postalCode"`
	PhoneNumber string `json:"phoneNumber"`
	Street      string `json:"street"`
}

type CalendarDatabaseRepresentation struct {
	ID          int
	UserID      int
	Title       string
	Description string
	DateFrom    time.Time
	DateTo      time.Time
}

type SessionFakturomaniaRepresentation struct {
	Value          string
	UserEmail      string
	UserLoginEmail string
	Valid          int
}

type CreateFakturomaniaContractor struct {
	Name       string
	Nip        string
	Street     string
	StreetSec  string
	PostalCode string
	PostalCity string
}

type FakturomaniaContractorResponse struct {
	ContractorId        int
	ContractorVersionId int
	Name                string
	NipPrefix           string
	Nip                 string
	Street              string
	PostalCode          string
	PostalCity          string
	CustomerAccountId   int
	SupplierAccountId   int
}

type FakturomaniaContractorRepresentation struct {
	ID                  int
	ContractorId        int `json:"contractorId"`
	ContractorVersionId int `json:"contractorVersionId"`
}

type SendEmail struct {
	Title   string   `json:"title"`
	Message string   `json:"message"`
	Emails  []string `json:"emails"`
}

type ChangePassword struct {
	Password           string `json:"password"`
	NewPassword        string `json:"newPassword"`
	RetypedNewPassword string `json:"retypedNewPassword"`
}

type FakturomaniaContractors struct {
	ID      int
	Name    string
	Surname string
}

type ContractorDetails struct {
	ID      int
	Name    string
	Surname string
}

type FrontendResponseInvoice struct {
	BankAccountNumber string
	BankName          string
	BuyerName         string
	Comments          string
	SellerName        string
	Contractor        ContractorDetails
	ProductsID        []int
}

type BuyerDetails struct {
	BankAccountNumber string
	BankName          string
	BuyerName         string
	Comments          string
	SellerName        string
}

type DocumentDetails struct {
	DocumentName         string
	DocumentNameIsCustom bool
	SaleDate             int64
	IssueDate            int64
}

type ProductInvoice struct {
	Ordinal            int     `json:"ordinal"`
	Name               string  `json:"name"`
	ClassificationName string  `json:"classificationName"`
	ClassificationCode string  `json:"classificationCode"`
	Unit               string  `json:"unit"`
	Quantity           float64 `json:"quantity"`
	NetPrice           float64 `json:"netPrice"`
	NetValue           float64 `json:"netValue"`
	VatRate            string  `json:"vatRate"`
	VatValue           float64 `json:"vatValue"`
	GrossValue         float64 `json:"grossValue"`
}

type PaymentInfo struct {
	PaymentDeadline int64   `json:"paymentDeadline"`
	PaymentMethod   string  `json:"paymentMethod"`
	PaymentStatus   string  `json:"paymentStatus"`
	AmountTotal     float64 `json:"amountTotal"`
	AmountPaid      float64 `json:"amountPaid"`
	PaymentDate     int64   `json:"paymentDate"`
}

type ContractorInvoices struct {
	ContractorId        int `json:"contractorId"`
	ContractorVersionId int `json:"contractorVersionId"`
}

type CreateInvoice struct {
	DocumentName         string             `json:"documentName"`
	DocumentNameIsCustom bool               `json:"documentNameIsCustom"`
	SaleDate             int64              `json:"saleDate"`
	IssueDate            int64              `json:"issueDate"`
	Contractor           ContractorInvoices `json:"contractor"`
	Products             []ProductInvoice   `json:"records"`
	Payment              PaymentInfo        `json:"paymentInfo"`
	BankAccountNumber    string             `json:"buyerName"`
	BankName             string             `json:"sellerName"`
	BuyerName            string             `json:"comments"`
	Comments             string             `json:"bankName"`
	SellerName           string             `json:"bankAccountNumber"`
}

type InvoiceDetails struct {
	InvoiceID int   `json:"invoiceId"`
	Created   int64 `json:"created"`
}

type InvoiceInfo struct {
	DocumentName string `json:"documentName"`
}

type ContractorInfo struct {
	ContractorId int `json:"contractorId"`
}

type Invoices struct {
	InvoiceDetails InvoiceDetails `json:"invoiceDetails"`
	InvoiceInfo    InvoiceInfo    `json:"invoiceInfo"`
	ContractorInfo ContractorInfo `json:"contractorInfo"`
}

type GetInvoices struct {
	Id                       int    `json:"id"`
	FakturomaniaContractorId int    `json:"fakturomaniaContractorId"`
	InvoiceId                int    `json:"invoiceId"`
	DocumentName             string `json:"documentName"`
	// CreatedAt                time.Time //`json:"createdAt"`
}

type Schedule struct {
	Id             int    `json:"id"`
	UserId         int    `json:"userId"`
	Name           string `json:"name"`
	Surname        string `json:"surname"`
	StartDayOfWork string `json:"startDayOfWork"`
	EndDayOfWork   string `json:"endDayOfWork"`
	Start          string `json:"start"`
	End            string `json:"end"`
}

type SpecificBillingAddressData struct {
	UserId int    `json:"userId"`
	Name   string `json:"name"`
}

type Role struct {
	Role int `json:"role"`
}

type UserDetails struct {
	Id                       int           `json:"id"`
	FakturomaniaContractorId sql.NullInt32 `json:"fakturomaniaContractorId"`
	Role                     int           `json:"role"`
	Email                    string        `json:"email"`
}

type QuestionnaireForm struct {
	Name     sql.NullString `json:"name"`
	Surname  sql.NullString `json:"surname"`
	Pesel    sql.NullString `json:"pesel"`
	Address  sql.NullString `json:"address"`
	Position sql.NullString `json:"position"`
}

type QuestionnaireFormUpdate struct {
	Name     string `json:"name"`
	Surname  string `json:"surname"`
	Pesel    string `json:"pesel"`
	Address  string `json:"address"`
	Position string `json:"position"`
}

type UserDetailsV2ToChange struct {
	Id                       int    `json:"id"`
	FakturomaniaContractorId int    `json:"fakturomaniaContractorId"`
	Role                     int    `json:"role"`
	Email                    string `json:"email"`
}

type UserTasks struct {
	Id    int    `json:"id"`
	Task  string `json:"task"`
	State bool   `json:"state"`
}
