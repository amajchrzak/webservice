package model

import (
	"database/sql"
	"fmt"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func UpdateQuestionnaire(questionnaire inferfaces.QuestionnaireFormUpdate, id int) int {
	query := `
		UPDATE questionnaire 
		SET name=$1, surname=$2, pesel=$3, address=$4, position=$5
		WHERE user_id=$6
	`

	_, err := DB.Exec(query,
		questionnaire.Name,
		questionnaire.Surname,
		questionnaire.Pesel,
		questionnaire.Address,
		questionnaire.Position,
		id,
	)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetProfileDetails(id int) (inferfaces.QuestionnaireForm, int) {
	var profile inferfaces.QuestionnaireForm
	// fmt.Println(id)
	query := `
		SELECT questionnaire.name, questionnaire.surname, questionnaire.pesel, questionnaire.address, questionnaire.position
		FROM questionnaire
		WHERE user_id=$1
	`

	rows := DB.QueryRow(query, id)

	err := rows.Scan(&profile.Name, &profile.Surname, &profile.Pesel, &profile.Address, &profile.Position)
	fmt.Println(err)
	switch {
	case err == sql.ErrNoRows:
		inferfaces.MESSAGE = "Brak informacji o użytkowniku"

		return profile, http.StatusOK
	case err != nil:
		inferfaces.MESSAGE = "Nie udało się pobrać informacji o użytkowniku!"

		return profile, http.StatusBadRequest
	}

	return profile, http.StatusOK
}

func GetTasks(id int) ([]inferfaces.UserTasks, int) {
	var tasks []inferfaces.UserTasks
	var task inferfaces.UserTasks

	query := `
		SELECT id, task, state
		FROM tasks
		WHERE user_id=$1 AND state = false
	`

	rows, err := DB.Query(query, id)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać zadań!"

		return tasks, http.StatusBadRequest
	}

	for rows.Next() {
		err = rows.Scan(&task.Id, &task.Task, &task.State)
		if err != nil {
			inferfaces.MESSAGE = "Nie udało się pobrać zadania!"

			return tasks, http.StatusBadRequest
		}

		tasks = append(tasks, task)
	}

	if err = rows.Err(); err != nil {
		inferfaces.MESSAGE = "Coś poszło nie tak"

		return tasks, http.StatusBadRequest
	}

	return tasks, http.StatusOK
}
