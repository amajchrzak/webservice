package model

import (
	"database/sql"
	"fmt"

	_ "github.com/lib/pq"
)

var DB *sql.DB
var err error

const ADMIN = "admin@admin.admin"

func ConnectDB() {
	// pgConnString := fmt.Sprintf("postgres://%v:%v@%v/%v?sslmode=disable",
	// 	os.Getenv("POSTGRES_USER"),
	// 	os.Getenv("POSTGRES_PASSWORD"),
	// 	os.Getenv("POSTGRES_HOST_AUTH_METHOD"),
	// 	os.Getenv("POSTGRES_DATABASE"))
	fmt.Println("connecting")
	// these details match the docker-compose.yml file.
	postgresInfo := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		"postgres", 5432, "user", "mypassword", "user")
	// fmt.Println(pgConnString)
	DB, err = sql.Open("postgres", postgresInfo)
	// DB, err = sql.Open("postgres", pgConnString)
	if err != nil {
		panic(err)
	}

	_, tableCheck := DB.Query("SELECT id FROM users LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table users exists")
	} else {
		_, err = DB.Exec(`CREATE TABLE users (
			id SERIAL PRIMARY KEY,
			email character varying(255) NOT NULL,
			password character varying(255) NOT NULL,
			role integer DEFAULT 0 NOT NULL,
			reset_password_token character varying(255),
			fakturomania_contractor_id integer
		)`)
		fmt.Println("create table: ", err)
		if err != nil {
			panic(err)
		}
		_, err = DB.Exec(`
			ALTER TABLE users 
			ADD UNIQUE (email);
		`)
		if err != nil {
			panic(err)
		}
	}

	_, tableCheck = DB.Query("SELECT id FROM questionnaire LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table questionnaire")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE questionnaire (
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			surname character varying(255) DEFAULT NULL,
			pesel character varying(255) DEFAULT NULL,
			address character varying(255) DEFAULT NULL,
			position character varying(255) DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM tasks LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table tasks")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE tasks (
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			task text NOT NULL,
			state boolean DEFAULT false NOT NULL
		);`)
	}

	_, tableCheck = DB.Query("SELECT id FROM billing_address LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table billing_address exists")
	} else {
		fmt.Println("Heja")
		_, err = DB.Exec(`
		CREATE TABLE billing_address(
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			surname character varying(255) DEFAULT NULL,
			address character varying(255) DEFAULT NULL,
			postal_code character varying(255) DEFAULT NULL,
			postal_city character varying(255) DEFAULT NULL,
			phone_number character varying(255) DEFAULT NULL,
			country character varying(255) DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM employees_schedule LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table employees_schedule exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE employees_schedule(
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			start_day_of_work character varying(255),
			end_day_of_work character varying(255),
			start_date date,
			end_date date
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM deliver LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table deliver exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE deliver(
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			description text DEFAULT NULL,
			city character varying(255) DEFAULT NULL,
			postal_code character varying(255) DEFAULT NULL,
			phone_number character varying(255) DEFAULT NULL,
			street character varying(255) DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM product LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table product exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE product(
			id SERIAL PRIMARY KEY,
			deliver_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			description text DEFAULT NULL,
			price integer DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM fakturomania_contractor LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table fakturomania_contractor exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE fakturomania_contractor(
			id SERIAL PRIMARY KEY,
			contractor_id integer NOT NULL,
			contractor_version_id integer NOT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM fakturomania_invoice LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table fakturomania_invoice exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE fakturomania_invoice(
			id SERIAL PRIMARY KEY,
			fakturomania_contractor_id integer DEFAULT NULL,
			invoice_id character varying(255) DEFAULT NULL,
			created_at date DEFAULT NULL,
			document_name character varying(255) DEFAULT NULL	
		)`)

		_, err = DB.Exec(`
		ALTER TABLE ONLY public.users
			ADD CONSTRAINT fakturomania_contractor_id 
			FOREIGN KEY (fakturomania_contractor_id) 
			REFERENCES public.fakturomania_contractor(id) 
			NOT VALID
		`)
		fmt.Println("alter table: ", err)
	}

	if rowExists("SELECT id FROM users WHERE email=$1", ADMIN) {
		fmt.Println("admin exists")
	} else {
		var userId struct {
			id int
		}
		_, err = DB.Exec("INSERT INTO users(email, password, role) VALUES('admin@admin.admin', 'b1234567890#', 3)")
		if err != nil {
			fmt.Println("insert err", err)
			panic(err)
		}

		err := DB.QueryRow("SELECT id FROM users WHERE email=$1", ADMIN).Scan(&userId.id)
		if err != nil {
			fmt.Println("select id err", err)

			panic(err)
		}
		fmt.Println("tutaj:", userId.id)

		_, err = DB.Exec("INSERT INTO billing_address(user_id, name, surname) VALUES($1, 'admin', 'admin')", userId.id)
		if err != nil {
			panic(err)
		}
	}

	// initDB()

	err = DB.Ping()
	fmt.Println(err)
	if err != nil {
		panic(err)
	}
}

func initDB() {
	fmt.Println("INIT DB!!!")
	// _, err := DB.Exec("GRANT ALL PRIVILEGES ON DATABASE usersy TO user")
	// if err != nil {
	// 	fmt.Println("Privileges!!!")
	// 	panic(err)
	// }
	_, tableCheck := DB.Query("SELECT id FROM users LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table users exists")
	} else {
		_, err = DB.Exec(`CREATE TABLE users (
			id SERIAL PRIMARY KEY,
			email character varying(255) NOT NULL,
			password character varying(255) NOT NULL,
			role integer DEFAULT 0 NOT NULL,
			reset_password_token character varying(255),
			fakturomania_contractor_id integer
		)`)
		fmt.Println("create table: ", err)
		if err != nil {
			panic(err)
		}
		_, err = DB.Exec(`
			ALTER TABLE users 
			ADD UNIQUE (email);
		`)
		if err != nil {
			panic(err)
		}
	}

	_, tableCheck = DB.Query("SELECT id FROM questionnaire LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table questionnaire")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE questionnaire (
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			surname character varying(255) DEFAULT NULL,
			pesel character varying(255) DEFAULT NULL,
			address character varying(255) DEFAULT NULL,
			position character varying(255) DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM tasks LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table tasks")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE tasks (
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			task text NOT NULL,
			state boolean DEFAULT false NOT NULL
		);`)
	}

	_, tableCheck = DB.Query("SELECT id FROM billing_address LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table billing_address exists")
	} else {
		fmt.Println("Heja")
		_, err = DB.Exec(`
		CREATE TABLE billing_address(
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			surname character varying(255) DEFAULT NULL,
			address character varying(255) DEFAULT NULL,
			postal_code character varying(255) DEFAULT NULL,
			postal_city character varying(255) DEFAULT NULL,
			phone_number character varying(255) DEFAULT NULL,
			country character varying(255) DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM employees_schedule LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table employees_schedule exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE employees_schedule(
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			start_day_of_work character varying(255),
			end_day_of_work character varying(255),
			start_date date,
			end_date date
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM deliver LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table deliver exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE deliver(
			id SERIAL PRIMARY KEY,
			user_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			description text DEFAULT NULL,
			city character varying(255) DEFAULT NULL,
			postal_code character varying(255) DEFAULT NULL,
			phone_number character varying(255) DEFAULT NULL,
			street character varying(255) DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM product LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table product exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE product(
			id SERIAL PRIMARY KEY,
			deliver_id integer NOT NULL,
			name character varying(255) DEFAULT NULL,
			description text DEFAULT NULL,
			price integer DEFAULT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM fakturomania_contractor LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table fakturomania_contractor exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE fakturomania_contractor(
			id SERIAL PRIMARY KEY,
			contractor_id integer NOT NULL,
			contractor_version_id integer NOT NULL
		)`)
	}

	_, tableCheck = DB.Query("SELECT id FROM fakturomania_invoice LIMIT 1")
	if tableCheck == nil {
		fmt.Println("Table fakturomania_invoice exists")
	} else {
		_, err = DB.Exec(`
		CREATE TABLE fakturomania_invoice(
			id SERIAL PRIMARY KEY,
			fakturomania_contractor_id integer DEFAULT NULL,
			invoice_id character varying(255) DEFAULT NULL,
			created_at date DEFAULT NULL,
			document_name character varying(255) DEFAULT NULL	
		)`)

		_, err = DB.Exec(`
		ALTER TABLE ONLY public.users
			ADD CONSTRAINT fakturomania_contractor_id 
			FOREIGN KEY (fakturomania_contractor_id) 
			REFERENCES public.fakturomania_contractor(id) 
			NOT VALID
		`)
		fmt.Println("alter table: ", err)
	}

	if rowExists("SELECT id FROM users WHERE email=$1", ADMIN) {
		fmt.Println("admin exists")
	} else {
		var userId struct {
			id int
		}
		_, err = DB.Exec("INSERT INTO users(email, password, role) VALUES('admin@admin.admin', 'b1234567890#', 3)")
		if err != nil {
			fmt.Println("insert err", err)
			panic(err)
		}

		err := DB.QueryRow("SELECT id FROM users WHERE email=$1", ADMIN).Scan(&userId.id)
		if err != nil {
			fmt.Println("select id err", err)

			panic(err)
		}
		fmt.Println("tutaj:", userId.id)

		_, err = DB.Exec("INSERT INTO billing_address(user_id, name, surname) VALUES($1, 'admin', 'admin')", userId.id)
		if err != nil {
			panic(err)
		}
	}
}

func rowExists(query string, args ...interface{}) bool {
	var exists bool
	query = fmt.Sprintf("SELECT exists (%s)", query)
	err := DB.QueryRow(query, args...).Scan(&exists)
	if err != nil && err != sql.ErrNoRows {
		// fmt.Sprintf("Error checking if row exists %s %v", args, err)
	}

	return exists
}

func CloseDB() {
	defer func() {
		if err := DB.Close(); err != nil {
			panic(err)
		}
	}()
}
