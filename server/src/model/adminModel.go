package model

import (
	"fmt"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func UpdateUserRole(id int, role int) int {
	query := `
		UPDATE users 
		SET role=$1
		WHERE id=$2
	`
	_, err := DB.Exec(query, role, id)
	fmt.Println(err)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się zmienić roli!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func InsertIntoQuestionnaireTable(id int) int {
	query := `
		INSERT INTO questionnaire(user_id)
		VALUES($1)
	`

	_, err := DB.Exec(query, id)
	if err != nil {
		inferfaces.MESSAGE = "Kwestionariusz dla użytkownika już istnieje"

		return http.StatusBadRequest
	}

	return http.StatusOK
}
