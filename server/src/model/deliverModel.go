package model

import (
	"database/sql"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func InsertDeliver(representation *inferfaces.DeliverDatabaseRepresentation) {
	err := DB.QueryRow("INSERT INTO deliver(name, description, city, postal_code, phone_number, street) "+
		"VALUES($1, $2, $3, $4, $5, $6) RETURNING id",
		representation.Name,
		representation.Description,
		representation.City,
		representation.PostalCode,
		representation.PhoneNumber,
		representation.Street).Scan(&representation.ID)
	if err != nil {
		panic(err)
	}
}

func DeleteDeliver(id int) {
	_, err := DB.Exec("DELETE FROM deliver WHERE id = $1", id)
	if err != nil {
		panic(err)
	}
}

func FindDeliverByID(id int) inferfaces.DeliverDatabaseRepresentation {
	rows := DB.QueryRow("SELECT * FROM deliver WHERE id = $1", id)

	deliver := inferfaces.DeliverDatabaseRepresentation{}

	err := rows.Scan(
		&deliver.ID,
		&deliver.Name,
		&deliver.Description,
		&deliver.City,
		&deliver.PostalCode,
		&deliver.PhoneNumber,
		&deliver.Street)
	switch {
	case err == sql.ErrNoRows:
		panic(err)
	case err != nil:
		panic(err)
	}

	return deliver
}

func GetDeliversList() ([]inferfaces.DeliverDatabaseRepresentation, int) {
	var deliver inferfaces.DeliverDatabaseRepresentation
	var delivers []inferfaces.DeliverDatabaseRepresentation

	rows, err := DB.Query("SELECT id, name, description, city, postal_code, phone_number, street FROM deliver")
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać listy dostawców!"

		return delivers, http.StatusInternalServerError
	}

	for rows.Next() {
		err = rows.Scan(&deliver.ID, &deliver.Name, &deliver.Description, &deliver.City, &deliver.PostalCode, &deliver.PhoneNumber, &deliver.Street)
		if err != nil {
			inferfaces.MESSAGE = "Nie udało się pobrać dostawcy!"

			return delivers, http.StatusBadRequest
		}

		delivers = append(delivers, deliver)
	}
	if err = rows.Err(); err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać listy dostawców!"

		return delivers, http.StatusBadRequest
	}

	return delivers, http.StatusOK
}

func UpdateDeliver(id int, representation *inferfaces.DeliverDatabaseRepresentation) {
	err := DB.QueryRow("UPDATE deliver "+
		"SET name = $1, description = $2, city = $3, postal_code = $4, phone_number = $5, street = $6"+
		"WHERE id = $7 RETURNING id",
		representation.Name,
		representation.Description,
		representation.City,
		representation.PostalCode,
		representation.PhoneNumber,
		representation.Street,
		id).Scan(&id)
	if err != nil {
		panic(err)
	}
	representation.ID = id
}
