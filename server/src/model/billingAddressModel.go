package model

import (
	"database/sql"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func InsertBillingAddress(representation *inferfaces.BillingAddressDatabaseRepresentation) {
	err := DB.QueryRow("INSERT INTO billing_address(user_id, name, surname, address, postal_code, postal_city, phone_number, country) "+
		"VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id",
		representation.UserID,
		representation.Name,
		representation.Surname,
		representation.Address,
		representation.PostalCode,
		representation.PostalCity,
		representation.PhoneNumber,
		representation.Country).Scan(&representation.ID)
	if err != nil {
		panic(err)
	}
}

func FindBillingAddressByID(id int) inferfaces.BillingAddressDatabaseRepresentation {
	rows := DB.QueryRow("SELECT * FROM billing_address WHERE user_id = $1", id)

	billingAddress := inferfaces.BillingAddressDatabaseRepresentation{}

	err := rows.Scan(
		&billingAddress.ID,
		&billingAddress.UserID,
		&billingAddress.Name,
		&billingAddress.Surname,
		&billingAddress.Address,
		&billingAddress.PostalCode,
		&billingAddress.PostalCity,
		&billingAddress.PhoneNumber,
		&billingAddress.Country)
	switch {
	case err == sql.ErrNoRows:
		panic(err)
	case err != nil:
		panic(err)
	}

	return billingAddress
}

func UpdateBillingAddress(billingAddress inferfaces.BillingAddressDatabaseRepresentation) {
	_, err := DB.Exec("UPDATE billing_address "+
		"SET name = $1, surname = $2, address = $3, postal_code = $4, postal_city = $5, phone_number = $6, country = $7"+
		"WHERE id = $8",
		billingAddress.Name,
		billingAddress.Surname,
		billingAddress.Address,
		billingAddress.PostalCode,
		billingAddress.PostalCity,
		billingAddress.PhoneNumber,
		billingAddress.Country,
		billingAddress.ID)
	if err != nil {
		panic(err)
	}
}

func GetBillingAddresses() ([]inferfaces.SpecificBillingAddressData, int) {
	var billingAddresses []inferfaces.SpecificBillingAddressData
	var billingAddress inferfaces.SpecificBillingAddressData

	query := `
		SELECT user_id, name 
		FROM billing_address
	`

	rows, err := DB.Query(query)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać adresów użytkowników"

		return billingAddresses, http.StatusBadRequest
	}

	for rows.Next() {
		err := rows.Scan(&billingAddress.UserId, &billingAddress.Name)
		if err != nil {
			inferfaces.MESSAGE = "Coś poszło nie tak"

			return billingAddresses, http.StatusBadRequest
		}

		billingAddresses = append(billingAddresses, billingAddress)
	}

	return billingAddresses, http.StatusOK
}

func InsertBillingAddressRegister(user *inferfaces.Register) int {
	_, err := DB.Exec("INSERT INTO billing_address(user_id, name, surname) "+
		"VALUES($1, $2, $3)",
		user.ID,
		user.Name,
		user.Surname)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się dodać adresu kontaktowego dla użytkownika!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}
