package model

import (
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func InsertTask(id int, formattedBody inferfaces.UserTasks) int {
	query := `
		INSERT INTO tasks(user_id, task) VALUES($1, $2)
	`

	_, err := DB.Exec(query, id, formattedBody.Task)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się dodać zadania!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func DeleteTask(id int) int {
	query := `
		UPDATE tasks SET state=true WHERE id=$1
	`

	_, err := DB.Exec(query, id)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się usunąć wiadomości"

		return http.StatusBadRequest
	}

	return http.StatusOK
}
