package model

import (
	"database/sql"
	"fmt"
	"net/http"

	"github.com/lib/pq"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func InsertUser(user *inferfaces.Register, hashedPassword string) int {
	_, err := DB.Exec("INSERT INTO users(email, password) VALUES($1, $2)", user.Email, hashedPassword)
	if err, ok := err.(*pq.Error); ok {
		fmt.Println(err)
		if err.Constraint == "users_email_key" {
			inferfaces.MESSAGE = "Email jest już zajęty"

			return http.StatusBadRequest
		}
	}

	return http.StatusOK
}

func FindOneBy(email string) (inferfaces.UserDatabaseRepresentation, int) {
	rows := DB.QueryRow("SELECT id, email, password, role, reset_password_token, fakturomania_contractor_id FROM users WHERE email = $1", email)

	user := inferfaces.UserDatabaseRepresentation{}

	err := rows.Scan(&user.ID, &user.Email, &user.Password, &user.Role, &user.UUID, &user.FakturomaniaContractorID)
	switch {
	case err == sql.ErrNoRows:
		inferfaces.MESSAGE = "Niepoprawne dane logowania!"

		return user, http.StatusBadRequest
	case err != nil:
		panic(err)
	}
	//todo obsluzyc errory
	return user, http.StatusOK
}

func UpdateResetPasswordToken(email string, genUUID string) {
	_, err := DB.Exec("UPDATE users SET reset_password_token = $1 WHERE email = $2", genUUID, email)
	if err != nil {
		panic(err)
	}
}

func FindByUUID(sentUUID string) inferfaces.UserDatabaseRepresentation {
	rows := DB.QueryRow("SELECT id, email, password, role, reset_password_token FROM users WHERE reset_password_token = $1", sentUUID)
	user := inferfaces.UserDatabaseRepresentation{}

	err := rows.Scan(&user.ID, &user.Email, &user.Password, &user.Role, &user.UUID)
	switch {
	case err == sql.ErrNoRows:
		panic(err)
	case err != nil:
		panic(err)
	}

	return user
}

func UpdatePassword(UUID sql.NullString, newPassword string) {
	// no rows in result set handle that
	// _, err := DB.Exec("UPDATE users SET password = $1, reset_password_token = $2 WHERE password = $3 AND reset_password_token = $4",
	// 	newPassword,
	// 	nil,
	// 	password,
	// 	UUID)
	_, err := DB.Exec("UPDATE users SET password = $1, reset_password_token = $2 WHERE reset_password_token = $3",
		newPassword,
		nil,
		UUID)
	if err != nil {
		panic(err)
	}
}

func ChangePassword(newPassword string, id int) {
	_, err := DB.Exec("UPDATE users SET password = $1 WHERE id = $2",
		newPassword,
		id)
	if err != nil {
		panic(err)
	}
}

func GetUsers() ([]inferfaces.ResponseUser, int) {
	users := []inferfaces.ResponseUser{}
	user := inferfaces.ResponseUser{}
	rows, err := DB.Query("SELECT id, email, role FROM users")
	if err != nil {

		return users, http.StatusBadRequest
	}

	for rows.Next() {
		err = rows.Scan(&user.ID, &user.Email, &user.Role)
		if err != nil {

			return users, http.StatusBadRequest
		}

		users = append(users, user)
	}

	if err = rows.Err(); err != nil {

		return users, http.StatusBadRequest
	}

	return users, http.StatusOK
}

func InsertToQuestionnaire(user *inferfaces.Register) int {
	query := `
		INSERT INTO questionnaire(user_id)
		VALUES($1)
	`
	_, err := DB.Exec(query, user.ID)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się dodać kwestionariusza..."

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func FindOneByID(id int) (inferfaces.UserDatabaseRepresentation, int) {
	query := `
		SELECT id, email, password, role
		FROM users WHERE id=$1
	`

	user := inferfaces.UserDatabaseRepresentation{}
	rows := DB.QueryRow(query, id)

	err := rows.Scan(&user.ID, &user.Email, &user.Password, &user.Role)
	fmt.Println(err)
	switch {
	case err == sql.ErrNoRows:
		panic(err)
	case err != nil:
		panic(err)
	}

	return user, http.StatusOK
}
