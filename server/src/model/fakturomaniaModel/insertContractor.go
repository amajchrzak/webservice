package fakturomaniaModel

import (
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func InsertFakturomaniaContractor(representation *inferfaces.FakturomaniaContractorRepresentation, idLoggedInUser int) {
	err := model.DB.QueryRow("INSERT INTO fakturomania_contractor(contractor_id, contractor_version_id) "+
		"VALUES($1, $2) RETURNING id",
		representation.ContractorId,
		representation.ContractorVersionId).Scan(&representation.ID)
	if err != nil {
		panic(err)
	}

	_, err = model.DB.Exec("UPDATE users SET fakturomania_contractor_id = $1 WHERE id = $2", representation.ID, idLoggedInUser)
	if err != nil {
		panic(err)
	}
}
