package fakturomaniaModel

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func InsertFakturomaniaInvoice(fakturomaniaContractorId int, invoice inferfaces.Invoices) int {
	timeT := time.Unix(invoice.InvoiceDetails.Created, 0)
	query := `
		INSERT INTO fakturomania_invoice(fakturomania_contractor_id, invoice_id, created_at, document_name)
		VALUES($1, $2, $3, $4)
	`

	_, err := model.DB.Exec(
		query,
		fakturomaniaContractorId,
		invoice.InvoiceDetails.InvoiceID,
		timeT,
		invoice.InvoiceInfo.DocumentName)
	fmt.Println(err)
	if err != nil {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func GetInvoices() ([]inferfaces.GetInvoices, int) {
	var invoices []inferfaces.GetInvoices
	var invoice inferfaces.GetInvoices

	query := `
		SELECT id, fakturomania_contractor_id, invoice_id, document_name
		FROM fakturomania_invoice
	`

	rows, err := model.DB.Query(query)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać informacji o fakturach!"

		return invoices, http.StatusBadRequest
	}

	for rows.Next() {
		err = rows.Scan(
			&invoice.Id,
			&invoice.FakturomaniaContractorId,
			&invoice.InvoiceId,
			// &invoice.CreatedAt,
			&invoice.DocumentName)
		if err != nil {
			inferfaces.MESSAGE = "Coś poszło nie tak DB!"

			return invoices, http.StatusBadRequest
		}

		invoices = append(invoices, invoice)
	}
	if err = rows.Err(); err != nil {
		inferfaces.MESSAGE = "Coś poszło nie tak!"

		return invoices, http.StatusBadRequest
	}

	return invoices, http.StatusOK
}
