package fakturomaniaModel

import (
	"database/sql"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func GetFakturomaniaContractors() ([]inferfaces.FakturomaniaContractors, int) {
	var fakturomaniaContractor inferfaces.FakturomaniaContractors
	var fakturomaniaContractors []inferfaces.FakturomaniaContractors

	query := `
		SELECT users.id, billing_address.name, billing_address.surname 
		FROM users 
		INNER JOIN billing_address ON users.id = billing_address.user_id 
		WHERE fakturomania_contractor_id IS NOT NULL
	`

	rows, err := model.DB.Query(query)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać listy kontrahentów"

		return fakturomaniaContractors, http.StatusBadRequest
	}

	for rows.Next() {
		err = rows.Scan(&fakturomaniaContractor.ID, &fakturomaniaContractor.Name, &fakturomaniaContractor.Surname)
		if err != nil {
			inferfaces.MESSAGE = "Coś poszło nie tak!"

			return fakturomaniaContractors, http.StatusBadRequest
		}

		fakturomaniaContractors = append(fakturomaniaContractors, fakturomaniaContractor)
	}

	if err = rows.Err(); err != nil {
		panic(err)
	}

	return fakturomaniaContractors, http.StatusOK
}

// INSERT INTO billing_address(user_id, name, surname, address, postal_code, postal_city, phone_number, country)
// VALUES('34', 'name', 'surname', 'address', 'postal_code', 'postal_city', 'phone_number', 'country')
func GetFakturomaniaContractorId(userID int) (inferfaces.FakturomaniaContractorRepresentation, int) {
	query := `
		SELECT fakturomania_contractor.contractor_id, fakturomania_contractor.contractor_version_id
		FROM users
		INNER JOIN fakturomania_contractor ON fakturomania_contractor.id = fakturomania_contractor_id
		WHERE users.id = $1
	`

	row := model.DB.QueryRow(query, userID)
	contractor := inferfaces.FakturomaniaContractorRepresentation{}

	err := row.Scan(&contractor.ContractorId, &contractor.ContractorVersionId)
	switch {
	case err == sql.ErrNoRows:

		return contractor, http.StatusBadRequest
	case err != nil:

		return contractor, http.StatusBadRequest
	}

	return contractor, http.StatusOK

}

func FindFakturomaniaContractorByID(contractorId int) (int, int) {
	query := `
		SELECT id 
		FROM fakturomania_contractor 
		WHERE contractor_id = $1
	`

	var userId int

	row := model.DB.QueryRow(query, contractorId)
	err := row.Scan(&userId)
	switch {
	case err == sql.ErrNoRows:

		return userId, http.StatusBadRequest
	case err != nil:

		return userId, http.StatusBadRequest
	}

	return userId, http.StatusOK
}
