package model

import (
	"fmt"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func AddNewEventToCalendar(representation *inferfaces.CalendarDatabaseRepresentation) {
	err := DB.QueryRow("INSERT INTO calendar(user_id, title, description, date_from, date_to) "+
		"VALUES($1, $2, $3, $4, $5) RETURNING id",
		representation.UserID,
		representation.Title,
		representation.Description,
		representation.DateFrom,
		representation.DateTo).Scan(&representation.ID)
	if err != nil {
		panic(err)
	}
}

func GetEmployeesSchedule() ([]inferfaces.Schedule, int) {
	var employeesSchedule []inferfaces.Schedule
	var employeeSchedule inferfaces.Schedule

	query := `
		SELECT 
			employees_schedule.id, employees_schedule.user_id, employees_schedule.start_day_of_work, 
			employees_schedule.end_day_of_work, employees_schedule.start_date, employees_schedule.end_date, 
			billing_address.name, billing_address.surname
		FROM employees_schedule 
		RIGHT JOIN billing_address ON employees_schedule.user_id = billing_address.user_id
		WHERE employees_schedule.user_id IS NOT NULL
	`
	rows, err := DB.Query(query)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać grafiku pracowników"

		return employeesSchedule, http.StatusBadRequest
	}

	for rows.Next() {
		err = rows.Scan(
			&employeeSchedule.Id,
			&employeeSchedule.UserId,
			&employeeSchedule.StartDayOfWork,
			&employeeSchedule.EndDayOfWork,
			&employeeSchedule.Start,
			&employeeSchedule.End,
			&employeeSchedule.Name,
			&employeeSchedule.Surname,
		)
		if err != nil {
			inferfaces.MESSAGE = "Coś poszło nie tak!"

			return employeesSchedule, http.StatusBadRequest
		}

		employeesSchedule = append(employeesSchedule, employeeSchedule)
	}

	if err = rows.Err(); err != nil {
		panic(err)
	}

	return employeesSchedule, http.StatusOK
}

func InsertUserToWorkSchedule(employee inferfaces.Schedule) int {
	employee.StartDayOfWork = string(employee.StartDayOfWork)
	employee.EndDayOfWork = string(employee.EndDayOfWork)

	// fmt.Printf("%T%T", employee.EndDayOfWork, employee.StartDayOfWork)
	// os.Exit(1)
	query := `
		INSERT INTO employees_schedule(user_id, start_day_of_work, end_day_of_work, start_date, end_date) 
		VALUES($1, $2, $3, $4, $5)
	`

	_, err := DB.Exec(query,
		&employee.UserId,
		&employee.StartDayOfWork,
		&employee.EndDayOfWork,
		&employee.Start,
		&employee.End,
	)
	fmt.Println(err)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się dodać osoby od grafiku!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}
