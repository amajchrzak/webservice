package model

import (
	"database/sql"
	"fmt"
	"net/http"
	"strconv"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func InsertProduct(representation *inferfaces.ProductDatabaseRepresentation) int {
	err := DB.QueryRow("INSERT INTO product(deliver_id ,name, description, price) "+
		"VALUES($1, $2, $3, $4) RETURNING id",
		representation.DeliverID,
		representation.Name,
		representation.Description,
		representation.Price).Scan(&representation.ID)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się dodać produktu!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func FindProductByID(id int) inferfaces.ProductDatabaseRepresentation {
	rows := DB.QueryRow("SELECT * FROM product WHERE id = $1", id)

	product := inferfaces.ProductDatabaseRepresentation{}

	err := rows.Scan(
		&product.ID,
		&product.Name,
		&product.Description,
		&product.Price,
		&product.DeliverID)
	switch {
	case err == sql.ErrNoRows:
		panic(err)
	case err != nil:
		panic(err)
	}

	return product
}

func GetProductList() ([]inferfaces.ProductDatabaseRepresentation, int) {
	var product inferfaces.ProductDatabaseRepresentation
	var products []inferfaces.ProductDatabaseRepresentation

	rows, err := DB.Query("SELECT id, name, description, price, deliver_id FROM product")
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się pobrać listy produktów!"

		return products, http.StatusBadRequest
	}

	for rows.Next() {
		err = rows.Scan(&product.ID, &product.Name, &product.Description, &product.Price, &product.DeliverID)
		if err != nil {

			return products, http.StatusBadRequest
		}

		products = append(products, product)
	}
	fmt.Println(products)
	if err = rows.Err(); err != nil {
		inferfaces.MESSAGE = "Coś poszło nie tak!"

		return products, http.StatusBadRequest
	}

	return products, http.StatusOK
}

func DeleteProduct(id int) {
	_, err := DB.Exec("DELETE FROM product WHERE id = $1", id)
	if err != nil {
		panic(err)
	}
}

func UpdateProduct(id int, representation *inferfaces.ProductDatabaseRepresentation) {
	err := DB.QueryRow("UPDATE product "+
		"SET name = $1, description = $2, price = $3, deliver_id = $4"+
		"WHERE id = $5 RETURNING id",
		representation.Name,
		representation.Description,
		representation.Price,
		representation.DeliverID,
		id).Scan(&id)
	if err != nil {
		panic(err)
	}
	representation.ID = id
}

func GetProductsByDeliverID(id int) []inferfaces.ProductDatabaseRepresentation {
	rows, err := DB.Query("SELECT * FROM product WHERE deliver_id = $1", id)
	if err != nil {
		panic(err)
	}

	var product inferfaces.ProductDatabaseRepresentation
	var products []inferfaces.ProductDatabaseRepresentation

	for rows.Next() {
		err = rows.Scan(&product.ID, &product.Name, &product.Description, &product.Price, &product.DeliverID)
		if err != nil {
			panic(err)
		}

		products = append(products, inferfaces.ProductDatabaseRepresentation{
			ID:          product.ID,
			DeliverID:   product.DeliverID,
			Name:        product.Name,
			Description: product.Description,
			Price:       product.Price})
	}
	if err = rows.Err(); err != nil {
		panic(err)
	}

	return products
}

func FindProductByIDReturnInvoiceRepresentation(id int) (inferfaces.ProductInvoice, int) {
	rows := DB.QueryRow("SELECT name, price FROM product WHERE id = $1", id)

	product := inferfaces.ProductInvoice{}

	err := rows.Scan(
		&product.Name,
		&product.GrossValue)

	switch {
	case err == sql.ErrNoRows:
		inferfaces.MESSAGE = "Nie ma takiego produktu!"

		return product, http.StatusBadRequest
	case err != nil:
		inferfaces.MESSAGE = "Coś poszło nie tak!"

		return product, http.StatusBadRequest
	}

	product.ClassificationName = inferfaces.CLASSIFICATION_NAME
	product.ClassificationCode = inferfaces.CLASSIFICATION_CODE
	product.Unit = inferfaces.UNIT
	product.Quantity = 1.00
	product.NetPrice = product.GrossValue * inferfaces.VAT_RATE / 100
	product.NetValue = product.NetPrice
	product.VatRate = strconv.Itoa(inferfaces.VAT_RATE) + "%"
	product.VatValue = product.GrossValue - product.NetValue

	return product, http.StatusOK
}
