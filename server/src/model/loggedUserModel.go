package model

import (
	"database/sql"
	"fmt"
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func FindOneById(id int) (inferfaces.UserDetails, int) {
	var userDetails inferfaces.UserDetails

	query := `
		SELECT id, fakturomania_contractor_id, role, email
		FROM users
		WHERE id = $1
	`

	rows := DB.QueryRow(query, id)
	err := rows.Scan(&userDetails.Id, &userDetails.FakturomaniaContractorId, &userDetails.Role, &userDetails.Email)
	fmt.Println(err)
	switch {
	case err == sql.ErrNoRows:

		return userDetails, http.StatusBadRequest
	case err != nil:

		return userDetails, http.StatusBadRequest
	}

	return userDetails, http.StatusOK
}
