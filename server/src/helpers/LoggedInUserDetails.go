package helpers

import (
	"net/http"
	"strings"

	"gitlab.com/amajchrzak/webservice/server/src/services/jwtTokenService"
)

func GetLoggedInUserID(r *http.Request) int {
	token := strings.Split(r.Header.Get("Authorization"), "Bearer ")
	loggedInUserID := jwtTokenService.ExtractClaims(token[1])
	if loggedInUserID == nil {

		return 0
	}

	return int(loggedInUserID.(float64))
}
