package jwtTokenService

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"

	"github.com/dgrijalva/jwt-go"
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

var jwtKey = []byte("TokenPassword")

func ExtractClaims(tokenStr string) interface{} {
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})

	if err != nil {
		return nil
	}

	if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
		return claims["user_id"]
	} else {
		log.Printf("Invalid JWT Token")
		return nil
	}
}

func PrepareJwtToken(user *inferfaces.Login) string {
	tokenContent := jwt.MapClaims{
		"user_id": user.ID,
		"expiry":  time.Now().Add(time.Minute * 60).Unix(),
	}
	jwtToken := jwt.NewWithClaims(jwt.SigningMethodHS256, tokenContent)
	token, err := jwtToken.SignedString(jwtKey)
	if err != nil {
		panic(err)
	}

	return token
}

func Middleware(handlerFunc http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		authHeader := strings.Split(r.Header.Get("Authorization"), "Bearer ")
		if len(authHeader) != 2 {
			http.Redirect(w, r, "/", http.StatusUnauthorized)
		} else {
			jwtToken := authHeader[1]
			token, err := jwt.Parse(jwtToken, func(token *jwt.Token) (interface{}, error) {
				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {

					return nil, fmt.Errorf("Unexpected signing method: %v", token.Header["alg"])
				}

				return jwtKey, nil
			})

			if claims, ok := token.Claims.(jwt.MapClaims); ok && token.Valid {
				ctx := context.WithValue(r.Context(), "props", claims)
				handlerFunc.ServeHTTP(w, r.WithContext(ctx))
			} else {
				fmt.Println(err)
				http.Redirect(w, r, "/", http.StatusUnauthorized)
			}
		}
	}
}
