package billingAddressService

import (
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func AddBillingAddress(billingAddress inferfaces.BillingAddressDatabaseRepresentation) map[string]interface{} {
	model.InsertBillingAddress(&billingAddress)

	return prepareDataToResponse(&billingAddress)
}

func prepareDataToResponse(billingAddress *inferfaces.BillingAddressDatabaseRepresentation) map[string]interface{} {
	var response = map[string]interface{}{"messsage": "The account has been registered"}
	response["data"] = billingAddress

	return response
}

func CheckRightUser(pathID int, loggedInUserID int, w http.ResponseWriter) map[string]interface{} {
	if pathID != loggedInUserID {
		http.Error(w, "Wrong user", 400)
		return map[string]interface{}{"message": "Wrong user"}
	}

	return map[string]interface{}{"message": "correct"}
}

func ChangeBillingAddressData(
	billingAddress *inferfaces.BillingAddressDatabaseRepresentation,
	requestBody *inferfaces.BillingAddressDatabaseRepresentation) map[string]interface{} {
	billingAddress.Name = requestBody.Name
	billingAddress.Surname = requestBody.Surname
	billingAddress.Address = requestBody.Address
	billingAddress.PostalCode = requestBody.PostalCode
	billingAddress.PostalCity = requestBody.PostalCity
	billingAddress.PhoneNumber = requestBody.PhoneNumber
	billingAddress.Country = requestBody.Country

	model.UpdateBillingAddress(*billingAddress)

	return prepareUpdatedDataToResponse(*billingAddress)
}

func prepareUpdatedDataToResponse(billingAddress inferfaces.BillingAddressDatabaseRepresentation) map[string]interface{} {
	var response = map[string]interface{}{"messsage": "Your billing address has been updated"}
	response["data"] = billingAddress

	return response
}
