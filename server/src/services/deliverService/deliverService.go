package deliverService

import (
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func AddDeliver(deliver inferfaces.DeliverDatabaseRepresentation) map[string]interface{} {
	model.InsertDeliver(&deliver)
	message := "Deliver has been added"

	return prepareDataToResponseDeliver(&deliver, message)
}

func prepareDataToResponseDeliver(representation *inferfaces.DeliverDatabaseRepresentation, message string) map[string]interface{} {
	var response = map[string]interface{}{"message": message}
	response["data"] = representation

	return response
}

func DeleteDeliverByID(id int) map[string]interface{} {
	model.DeleteDeliver(id)
	var response = map[string]interface{}{"message": "Deliver successfully deleted"}

	return response
}

func GetDeliverByID(id int) map[string]interface{} {
	deliver := model.FindDeliverByID(id)
	message := "Deliver successfully returned"

	return prepareDataToResponseDeliver(&deliver, message)
}

func GetDelivers() ([]inferfaces.DeliverDatabaseRepresentation, int) {
	delivers, status := model.GetDeliversList()
	if status != http.StatusOK {

		return delivers, http.StatusBadRequest
	}

	return delivers, http.StatusOK
}

func UpdateDeliverByID(id int, request inferfaces.DeliverDatabaseRepresentation) map[string]interface{} {
	model.UpdateDeliver(id, &request)
	message := "Deliver successfully updated"

	return prepareDataToResponseDeliver(&request, message)
}
