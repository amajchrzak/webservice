package fakturomaniaService

import (
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model/fakturomaniaModel"
)

func AddFakturomaniaContractor(fakturomaniaContractor *inferfaces.FakturomaniaContractorRepresentation, idLoggedInUser int) {
	fakturomaniaModel.InsertFakturomaniaContractor(fakturomaniaContractor, idLoggedInUser)
}
