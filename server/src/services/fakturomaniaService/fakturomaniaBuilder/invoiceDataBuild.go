package fakturomaniabuilder

import (
	"encoding/json"
	"net/http"
	"time"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
	"gitlab.com/amajchrzak/webservice/server/src/model/fakturomaniaModel"
)

func Build(body []byte) ([]byte, int) {
	var products []inferfaces.ProductInvoice
	var paymentInfo inferfaces.PaymentInfo
	// var restData inferfaces.DocumentDetails

	// fmt.Println(bytes.NewBuffer(body))

	var formattedBody inferfaces.FrontendResponseInvoice
	err := json.Unmarshal(body, &formattedBody)
	if err != nil {

		return body, http.StatusBadRequest
	}

	contractorDetails, status := fakturomaniaModel.GetFakturomaniaContractorId(formattedBody.Contractor.ID)
	if status != http.StatusOK {

		return body, http.StatusBadRequest
	}
	for i := 0; i < len(formattedBody.ProductsID); i++ {
		product, status := model.FindProductByIDReturnInvoiceRepresentation(formattedBody.ProductsID[i])
		if status != http.StatusOK {

			return body, http.StatusBadRequest
		}
		product.Ordinal = i + 1
		// paymentInfo.AmountTotal += product.GrossValue

		products = append(products, product)
	}

	// for i := 0; i < len(products); i++ {
	// 	fmt.Println("Ordinal", products[i].Ordinal)
	// 	fmt.Println("Name", products[i].Name)
	// 	fmt.Println("ClassificationName", products[i].ClassificationName)
	// 	fmt.Println("ClassificationCode", products[i].ClassificationCode)
	// 	fmt.Println("Unit", products[i].Unit)
	// 	fmt.Println("Quantity", products[i].Quantity)
	// 	fmt.Println("NetPrice", products[i].NetPrice)
	// 	fmt.Println("NetValue", products[i].NetValue)
	// 	fmt.Println("VatRate: ", products[i].VatRate)
	// 	fmt.Println("VatValue: ", products[i].VatValue)
	// 	fmt.Println("GrossValue: ", products[i].GrossValue)
	// }
	paymentInfo.PaymentDeadline = time.Now().Add(7*24*time.Hour).Unix() * 1000
	paymentInfo.PaymentDate = time.Now().Unix() * 1000
	paymentInfo.PaymentStatus = inferfaces.PAYMENT_STATUS_UNPAID
	paymentInfo.AmountPaid = 0
	paymentInfo.PaymentMethod = inferfaces.PAYMENT_METHOD
	// restData.DocumentName = ""
	// restData.DocumentNameIsCustom = false
	// restData.SaleDate = time.Now().Unix() * 1000
	// restData.IssueDate = time.Now().Unix() * 1000
	// fmt.Println(contractorDetails.ContractorId, contractorDetails.ContractorVersionId) // contractor wzięty fest
	//documentName = ""
	//documentNameIsCuston = false
	//issue date to samo
	// fmt.Println(
	// 	restData.DocumentName,
	// 	restData.DocumentNameIsCustom,
	// 	restData.SaleDate,
	// 	restData.IssueDate,
	// 	contractorDetails.ContractorId,
	// 	contractorDetails.ContractorVersionId)
	// for i := 0; i < len(products); i++ {
	// 	fmt.Println(products[i].Ordinal,
	// 		products[i].Name,
	// 		products[i].ClassificationName,
	// 		products[i].ClassificationCode,
	// 		products[i].Unit,
	// 		products[i].Quantity,
	// 		products[i].NetPrice,
	// 		products[i].NetValue,
	// 		products[i].VatRate,
	// 		products[i].VatValue,
	// 		products[i].GrossValue)
	// }
	// fmt.Println(
	// 	paymentInfo.PaymentDeadline,
	// 	paymentInfo.PaymentMethod,
	// 	paymentInfo.PaymentStatus,
	// 	paymentInfo.AmountTotal,
	// 	paymentInfo.AmountPaid,
	// 	paymentInfo.PaymentDate)
	// fmt.Println(
	// 	formattedBody.BuyerName,
	// 	formattedBody.SellerName,
	// 	formattedBody.Comments,
	// 	formattedBody.BankName,
	// 	formattedBody.BankAccountNumber)

	var request inferfaces.CreateInvoice
	var tmp inferfaces.ProductInvoice
	var tmpAmountToPay float64
	request.DocumentName = ""
	request.DocumentNameIsCustom = false
	request.SaleDate = time.Now().Unix() * 1000
	request.IssueDate = time.Now().Unix() * 1000
	request.Contractor.ContractorId = contractorDetails.ContractorId
	request.Contractor.ContractorVersionId = contractorDetails.ContractorVersionId
	for i := 0; i < len(products); i++ {
		tmp.Ordinal = products[i].Ordinal + 1
		tmp.Name = products[i].Name
		tmp.ClassificationName = products[i].ClassificationName
		tmp.ClassificationCode = products[i].ClassificationCode
		tmp.Unit = products[i].Unit
		tmp.Quantity = products[i].Quantity
		tmp.NetPrice = products[i].NetPrice
		tmp.NetValue = products[i].NetValue
		tmp.VatRate = products[i].VatRate
		tmp.VatValue = products[i].VatValue
		tmp.GrossValue = products[i].GrossValue

		tmpAmountToPay += products[i].GrossValue

		request.Products = append(request.Products, tmp)
	}
	request.Payment.PaymentDeadline = time.Now().Add(7*24*time.Hour).Unix() * 1000
	request.Payment.PaymentDate = time.Now().Unix() * 1000
	request.Payment.PaymentStatus = inferfaces.PAYMENT_STATUS_UNPAID
	request.Payment.AmountPaid = 0
	request.Payment.PaymentMethod = inferfaces.PAYMENT_METHOD
	request.BuyerName = formattedBody.BuyerName
	request.SellerName = formattedBody.SellerName
	request.Comments = formattedBody.Comments
	request.BankName = formattedBody.BankName
	request.BankAccountNumber = formattedBody.BankAccountNumber
	request.Payment.AmountTotal = tmpAmountToPay
	requestByte, _ := json.Marshal(request)

	return requestByte, http.StatusOK
}

// {
// 	"documentName":"dsfafsdafd",
// 	"documentNameIsCustom":false,
// 	"saleDate":1503885600000,
// 	"issueDate":1503885600000,
// 	"contractor":{
// 	   "contractorId":146648,
// 	   "contractorVersionId":336137
// 	},
// 	"records":[
// 	   {
// 		 "ordinal": 1,
// 		 "name": "Najem lokalu za miesiąc czerwiec",
// 		 "classificationName": "PKWIU",
// 		 "classificationCode": "333",
// 		 "unit": "szt",
// 		 "quantity": 1.00,
// 		 "netPrice": 800.00,
// 		 "netValue": 800.00,
// 		 "vatRate": "23%",
// 		 "vatValue": 184.00,
// 		 "grossValue": 984.00
// 	   },
// 	   {
// 		 "ordinal": 2,
// 		 "name": "Najem lokalu za miesiąc lipiec",
// 		 "classificationName": "PKWIU",
// 		 "classificationCode": "333",
// 		 "unit": "szt",
// 		 "quantity": 1.00,
// 		 "netPrice": 800.00,
// 		 "netValue": 800.00,
// 		 "vatRate": "23%",
// 		 "vatValue": 184.00,
// 		 "grossValue": 984.00
// 	   }
// 	],
// 	"paymentInfo":{
// 	   "paymentDeadline":1503885600000,
// 	   "paymentMethod":"CASH",
// 	   "paymentStatus":"PAID",
// 	   "amountPaid":135,
// 	   "paymentDate":1503885600000
// 	},
// 	"buyerName":"Jan Kowalski",
// 	"sellerName":"Sławomir Nowak",
// 	"comments":"towary handlowe",
// 	"bankName":"Bank Testowy",
// 	"bankAccountNumber":"33 3333 3333 3333 3333 3333 3333"
//  }
