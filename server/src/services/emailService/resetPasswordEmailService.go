package emailService

import (
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/helpers"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func StartResetPasswordProcedureEmail(email string) int {
	genUUID := helpers.NewUUID()
	model.UpdateResetPasswordToken(email, genUUID)

	status := sendResetPasswordEmail(email, genUUID)
	if status != http.StatusOK {

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func sendResetPasswordEmail(email string, genUUID string) int {
	// auth := smtp.PlainAuth("", "1ce0c31fbeecf7", "b553c53b547af5", "smtp.mailtrap.io")

	// to := []string{email}

	// message := []byte("From: pracainzynierskamaile@gmail.com\r\n" +
	// 	"To: " + email + "\r\n" +
	// "Subject: Reset Password\r\n" +
	// "\r\n" +
	// "Reset Password Token: \r\n" + genUUID)
	// err := smtp.SendMail("smtp.mailtrap.io:2525", auth, "pracainzynierskamaile@gmail.com", to, message)
	// if err != nil {
	// 	inferfaces.MESSAGE = "Nie udało sie wysłać maila!"

	// 	return http.StatusBadRequest
	// }

	return http.StatusOK
}
