package emailService

import (
	"net/http"
	"net/smtp"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
)

func SendGroupEmail(emails inferfaces.SendEmail) int {
	receivers := setReceivers(emails.Emails)
	to, status := prepareMailingList(receivers)
	if status != http.StatusOK {
		inferfaces.MESSAGE = "Nie udało się pobrać listy mailingowej!"

		return http.StatusBadRequest
	}

	auth := smtp.PlainAuth(inferfaces.EMAIL_IDENTITY, inferfaces.EMAIL_USERNAME, inferfaces.EMAIL_PASSWORD, inferfaces.EMAIL_HOST)
	msg := []byte("From: " + inferfaces.EMAIL_FROM + "\r\n" +
		"To: " + to + "\r\n" +
		"Subject: " + emails.Title + "\r\n" +
		"\r\n" +
		emails.Message + "\r\n")

	err := smtp.SendMail(inferfaces.EMAIL_ADDRESS, auth, inferfaces.EMAIL_FROM, receivers, msg)
	if err != nil {
		inferfaces.MESSAGE = "Nie udało się wysłać emaila!"

		return http.StatusBadRequest
	}

	return http.StatusOK
}

func setReceivers(groupEmailDetails []string) []string {
	receivers := make([]string, len(groupEmailDetails))
	for index := range groupEmailDetails {
		receivers[index] = groupEmailDetails[index]
	}

	return receivers
}

func prepareMailingList(receivers []string) (string, int) {
	var listOfReceivers string

	for index := range receivers {
		if index >= len(receivers)-1 {
			listOfReceivers += receivers[index]
		} else {
			listOfReceivers += receivers[index] + ", "
		}
	}

	if len(listOfReceivers) == 0 {

		return listOfReceivers, http.StatusBadRequest
	}

	return listOfReceivers, http.StatusOK
}
