package userService

import (
	"net/http"
	"regexp"
	"time"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
	"gitlab.com/amajchrzak/webservice/server/src/services/jwtTokenService"
	"golang.org/x/crypto/bcrypt"
)

var emailRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
var passwordRegex = regexp.MustCompile("[^\\w\\d]*(([0-9]+.*[A-Za-z]+.*)|[A-Za-z]+.*([0-9]+.*))")

func hashPassword(password []byte) string {
	hashedPassword, err := bcrypt.GenerateFromPassword(password, bcrypt.MinCost)
	if err != nil {
		panic(err)
	}

	return string(hashedPassword)
}

func PrepareDataToRegister(user *inferfaces.Register) (*inferfaces.Register, int) {
	if !checkIfPasswordsMatch(user.Password, user.RetypedPassword) {
		inferfaces.MESSAGE = "Hasła różnią się!"

		return user, http.StatusBadRequest
	}

	if !isEmailValid(user.Email) {
		inferfaces.MESSAGE = "Nieprawidłowy Email!"

		return user, http.StatusBadRequest
	}

	if !isPasswordValid(user.Password) {
		inferfaces.MESSAGE = "Nieprawidłowe hasło!"

		return user, http.StatusBadRequest
	}

	status := model.InsertUser(user, hashPassword([]byte(user.Password)))
	if status != http.StatusOK {

		return user, http.StatusBadRequest
	}

	return user, http.StatusOK
}

func checkIfPasswordsMatch(password string, retypedPassword string) bool {
	if password != retypedPassword {
		return false
	}

	return true
}

func PrepareDataToLogin(user inferfaces.Login) (inferfaces.Login, string, int) {
	inferfaces.MESSAGE = "SUCCESS"
	foundUser, status := findUserByEmail(user.Email)
	if status != http.StatusOK {

		return inferfaces.Login{}, "", http.StatusBadRequest
	}

	if !comparePassword(foundUser.Password, user.Password) {
		inferfaces.MESSAGE = "Niepoprawne hasło"

		return user, "", http.StatusBadRequest
	}

	token := jwtTokenService.PrepareJwtToken(&foundUser)

	return foundUser, token, http.StatusOK
}

func isEmailValid(email string) bool {
	if len(email) < 3 && len(email) > 254 {
		return false
	}

	return emailRegex.MatchString(email)
}

func isPasswordValid(password string) bool {
	if len(password) < 3 && len(password) > 254 {
		return false
	}

	return passwordRegex.MatchString(password)
}

func findUserByEmail(email string) (inferfaces.Login, int) {
	user, status := model.FindOneBy(email)
	if status != http.StatusOK {

		return inferfaces.Login{}, http.StatusBadRequest
	}
	loginData := inferfaces.Login{ID: user.ID, Email: user.Email, Password: user.Password, Role: user.Role}

	return loginData, http.StatusOK
}

func comparePassword(password string, loginPassword string) bool {
	passwordErr := bcrypt.CompareHashAndPassword([]byte(password), []byte(loginPassword))

	if passwordErr == bcrypt.ErrMismatchedHashAndPassword && passwordErr != nil {
		return false
	}

	return true
}

func CheckRequestCorrectness(newPassword string, retypedNewPassword string, token string) int {
	if !checkIfPasswordsMatch(newPassword, retypedNewPassword) {
		inferfaces.MESSAGE = "Password doesn't match"

		return http.StatusBadRequest
	}

	if !isPasswordValid(newPassword) {
		inferfaces.MESSAGE = "To weak password"

		return http.StatusBadRequest
	}

	hashedNewPassword := hashPassword([]byte(newPassword))
	user := model.FindByUUID(token)
	model.UpdatePassword(user.UUID, hashedNewPassword)

	return http.StatusOK
}

func SetCookieToken(w http.ResponseWriter, token string, duration time.Time, cookieName string) {
	cookie := http.Cookie{
		Name:     cookieName,
		Value:    token,
		Expires:  duration,
		HttpOnly: false,
	}

	http.SetCookie(w, &cookie)
}

func ChangePasswordValidation(hashOldPassword, oldPassword, newPassword, retypedNewPassword string, id int) int {
	if !comparePassword(hashOldPassword, oldPassword) {
		inferfaces.MESSAGE = "Niepoprawne hasło"

		return http.StatusBadRequest
	}

	if !checkIfPasswordsMatch(newPassword, retypedNewPassword) {
		inferfaces.MESSAGE = "Password doesn't match"

		return http.StatusBadRequest
	}

	if !isPasswordValid(newPassword) {
		inferfaces.MESSAGE = "To weak password"

		return http.StatusBadRequest
	}

	hashedNewPassword := hashPassword([]byte(newPassword))
	model.ChangePassword(hashedNewPassword, id)

	return http.StatusOK
}
