package calendarService

import (
	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func CreateNewEvent(representation inferfaces.CalendarDatabaseRepresentation) map[string]interface{} {
	model.AddNewEventToCalendar(&representation)
	message := "Event has been added"

	return prepareDataToResponseCalendar(&representation, message)
}

func prepareDataToResponseCalendar(representation *inferfaces.CalendarDatabaseRepresentation, message string) map[string]interface{} {
	var response = map[string]interface{}{"message": message}
	response["data"] = representation

	return response
}
