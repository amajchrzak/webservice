package productService

import (
	"net/http"

	"gitlab.com/amajchrzak/webservice/server/src/inferfaces"
	"gitlab.com/amajchrzak/webservice/server/src/model"
)

func AddProduct(product inferfaces.ProductDatabaseRepresentation) (inferfaces.ProductDatabaseRepresentation, int) {
	status := model.InsertProduct(&product)
	if status != http.StatusOK {

		return product, http.StatusBadRequest
	}

	return product, http.StatusOK
}

func prepareDataToResponseProduct(representation *inferfaces.ProductDatabaseRepresentation, message string) map[string]interface{} {
	var response = map[string]interface{}{"message": message}
	response["data"] = representation

	return response
}

func GetProductByID(id int) map[string]interface{} {
	product := model.FindProductByID(id)
	message := "Product successfully returned"

	return prepareDataToResponseProduct(&product, message)
}

func GetProducts() ([]inferfaces.ProductDatabaseRepresentation, int) {
	products, status := model.GetProductList()
	if status != http.StatusOK {

		return products, http.StatusBadRequest
	}

	return products, http.StatusOK
}

func DeleteProductByID(id int) map[string]interface{} {
	model.DeleteProduct(id)
	var response = map[string]interface{}{"message": "product successfully deleted"}

	return response
}

func UpdateProductByID(id int, request inferfaces.ProductDatabaseRepresentation) map[string]interface{} {
	model.UpdateProduct(id, &request)
	message := "Products successfully returned"

	return prepareDataToResponseProduct(&request, message)
}

func GetProductsFromSpecifiedDeliver(id int) ([]inferfaces.ProductDatabaseRepresentation, int) {
	products := model.GetProductsByDeliverID(id)

	return products, http.StatusOK
}
