package routes

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
	"gitlab.com/amajchrzak/webservice/server/src/controllers"
	"gitlab.com/amajchrzak/webservice/server/src/controllers/user"
	"gitlab.com/amajchrzak/webservice/server/src/services/jwtTokenService"
)

func StartApi() {
	router := mux.NewRouter()

	router.HandleFunc("/", controllers.Index).Methods("GET", "OPTIONS")

	router.HandleFunc("/register", user.Register).Methods("POST", "OPTIONS")
	router.HandleFunc("/login", user.Login).Methods("POST", "OPTIONS")
	router.HandleFunc("/logout", user.Logout).Methods("POST", "OPTIONS")
	router.HandleFunc("/send-reset-password-procedure", user.SendResetPasswordProcedure).Methods("POST", "OPTIONS")
	router.HandleFunc("/reset-password", user.ResetPassword).Methods("POST", "OPTIONS")
	router.HandleFunc("/change-password/{id}", user.ChangePassword).Methods("POST", "OPTIONS")

	router.HandleFunc("/billing_address", jwtTokenService.Middleware(controllers.CreateBillingAddress)).Methods("POST")
	router.HandleFunc("/update_billing_address/{id}", jwtTokenService.Middleware(controllers.UpdateBillingAddress)).Methods("PUT")
	router.HandleFunc("/get-billing-addresses", controllers.GetBillingAddresses).Methods("GET", "OPTIONS")

	router.HandleFunc("/add-product", controllers.AddProduct).Methods("POST", "OPTIONS")
	router.HandleFunc("/get_product/{id}", jwtTokenService.Middleware(controllers.GetProduct)).Methods("GET")
	router.HandleFunc("/get-products", controllers.GetAllProducts).Methods("GET", "OPTIONS")
	router.HandleFunc("/delete_product/{id}", jwtTokenService.Middleware(controllers.DeleteProduct)).Methods("DELETE")
	router.HandleFunc("/update_product/{id}", jwtTokenService.Middleware(controllers.UpdateProduct)).Methods("PUT")
	router.HandleFunc("/get_products/{id}/deliver", jwtTokenService.Middleware(controllers.GetProductByDeliverID)).Methods("GET")

	router.HandleFunc("/add_deliver", jwtTokenService.Middleware(controllers.AddDeliver)).Methods("POST")
	router.HandleFunc("/delete_deliver/{id}", jwtTokenService.Middleware(controllers.DeleteDeliver)).Methods("DELETE")
	router.HandleFunc("/get_deliver/{id}", jwtTokenService.Middleware(controllers.GetDeliver)).Methods("GET")
	router.HandleFunc("/get-delivers", controllers.GetAllDelivers).Methods("GET", "OPTIONS")
	router.HandleFunc("/update_deliver/{id}", jwtTokenService.Middleware(controllers.UpdateDeliver)).Methods("PUT")

	router.HandleFunc("/insert-user-to-work-schedule", controllers.InsertUserToWorkSchedule).Methods("POST", "OPTIONS")
	router.HandleFunc("/get-employees-schedule", controllers.GetEmployeesSchedule).Methods("GET", "OPTIONS")

	router.HandleFunc("/create-contractor", controllers.CreateFakturomaniaContractor).Methods("POST", "OPTIONS")
	router.HandleFunc("/get_contractor/{id}", jwtTokenService.Middleware(controllers.GetFakturomaniaContractorByUserId)).Methods("GET")
	router.HandleFunc("/create-invoice", controllers.CreateInvoice).Methods("POST", "OPTIONS")
	router.HandleFunc("/get-contractors", controllers.GetFakturomaniaContractors).Methods("GET", "OPTIONS")
	router.HandleFunc("/get-fakturomania-invoice", controllers.GetFakturomaniaInvoice).Methods("GET", "OPTIONS")
	router.HandleFunc("/get-invoice-by-id/{invoiceId}", controllers.GetInvoiceById).Methods("GET", "OPTIONS")
	router.HandleFunc("/get-invoices", controllers.GetInvoices).Methods("GET", "OPTIONS")

	router.HandleFunc("/send-email", controllers.SendMail).Methods("POST", "OPTIONS")

	router.HandleFunc("/get-users", user.GetUsers).Methods("GET", "OPTIONS")
	router.HandleFunc("/update-role/{userId}", controllers.UpdateRole).Methods("PATCH", "OPTIONS")
	router.HandleFunc("/get-logged-in-user-details", controllers.GetLoggedUserDetails).Methods("GET", "OPTIONS")

	router.HandleFunc("/update-questionnaire", controllers.UpdateQuestionnaire).Methods("PATCH", "OPTIONS")
	router.HandleFunc("/get-profile-details", controllers.GetProfileDetails).Methods("GET", "OPTIONS")
	router.HandleFunc("/get-specific-user-details/{userId}", user.GetSpecificUserDetails).Methods("GET", "OPTIONS")

	router.HandleFunc("/add-task/{id}", controllers.AddTask).Methods("POST", "OPTIONS")
	router.HandleFunc("/delete-task/{id}", controllers.DeleteTask).Methods("PATCH", "OPTIONS")
	// tasks are returned with specific user details. so endpoint is not needed

	log.Fatal(http.ListenAndServe(":8080", router))
}
