package main

import (
	_ "github.com/lib/pq"
	"gitlab.com/amajchrzak/webservice/server/src/model"
	"gitlab.com/amajchrzak/webservice/server/src/routes"
)

func main() {
	model.ConnectDB()
	routes.StartApi()
	model.CloseDB()
}
