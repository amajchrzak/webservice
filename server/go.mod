module gitlab.com/amajchrzak/webservice/server

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/mux v1.8.0
	github.com/kr/pretty v0.1.0 // indirect
	github.com/lib/pq v1.10.2
	github.com/satori/go.uuid v1.2.0
	golang.org/x/crypto v0.0.0-20210513164829-c07d793c2f9a
	gopkg.in/check.v1 v1.0.0-20180628173108-788fd7840127 // indirect
)
