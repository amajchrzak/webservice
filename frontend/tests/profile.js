const { Builder, Key, By, WebElement } = require("selenium-webdriver");

async function profile () {
    let driver = await new Builder().forBrowser("firefox").build();

     // open web browser and go to localhost:3000
     await driver.get("http://localhost:3000/");
     // find login button in nav and move to login page
     await driver.sleep(1000);
     await driver.findElement(By.xpath("/html/body/div/div/nav/div/div/ul/li[1]/a")).click();  
     // set login as login@login
     await driver.sleep(1000);
     await driver.findElement(By.xpath("/html/body/div/div/main/div/form/input[1]")).sendKeys("adam10@adam.adam", Key.RETURN);
     // set password as password
     await driver.sleep(1000);
     await driver.findElement(By.xpath("/html/body/div/div/main/div/form/input[2]")).sendKeys("b1234567890#", Key.RETURN);

    await driver.sleep(1000);   
    let profileButton = await driver.findElement(By.id("profile-button"))
    await profileButton.click();

    await updateQuestionnaire(driver);
    await driver.sleep(1000);
    await changePassword(driver);
    await driver.sleep(1000);

    
}

async function updateQuestionnaire(driver) {
    await driver.findElement(By.id("form-update-questionnaire")).click();
    await driver.sleep(1000);
    await driver.findElement(By.id("questionnaire-name")).sendKeys("Adam", Key.RETURN);
    await driver.sleep(500);
    await driver.findElement(By.id("questionnaire-surname")).sendKeys("Majchrzak", Key.RETURN);
    await driver.sleep(500);
    await driver.findElement(By.id("questionnaire-pesel")).sendKeys("1122334455", Key.RETURN);
    await driver.sleep(500);
    await driver.findElement(By.id("questionnaire-address")).sendKeys("Szkolna", Key.RETURN);
    await driver.sleep(500);
    await driver.findElement(By.id("questionnaire-position")).sendKeys("Developer", Key.RETURN);
    await driver.sleep(500);
}

async function changePassword(driver) {
    await driver.findElement(By.id("form-change-password")).click();
    await driver.sleep(1000);
    await driver.findElement(By.id("change-password-password")).sendKeys("b1234567890#", Key.RETURN);
    await driver.sleep(500);
    await driver.findElement(By.id("change-password-new-password")).sendKeys("b1234567890#", Key.RETURN);
    await driver.sleep(500);
    await driver.findElement(By.id("change-password-retyped-new-password")).sendKeys("b1234567890#", Key.RETURN);
    await driver.sleep(500);
}

profile().catch(
    console.log
);