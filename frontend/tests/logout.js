const { Builder, By, Key } = require ("selenium-webdriver");

async function logout() {
    let driver = await new Builder().forBrowser("firefox").build();

    // open web browser and go to localhost:3000
    await driver.get("http://localhost:3000/");
    // find login button in nav and move to login page
    await driver.findElement(By.xpath("/html/body/div/div/nav/div/div/ul/li[1]/a")).click();  
    // set login 
    await driver.findElement(By.xpath("/html/body/div/div/main/div/form/input[1]")).sendKeys("adam10@adam.adam", Key.RETURN);
    // set password
    await driver.findElement(By.xpath("/html/body/div/div/main/div/form/input[2]")).sendKeys("b1234567890#", Key.RETURN);
    // click Login in
    await driver.findElement(By.xpath("/html/body/div/div/main/div/form/button")).click();
    // wait 2s
    await driver.sleep(2000);
    
    // find logout button in nav and move to logout page
    await driver.findElement(By.xpath("/html/body/div/div/nav/div/div/ul/li[7]/a")).click();  
}

logout();