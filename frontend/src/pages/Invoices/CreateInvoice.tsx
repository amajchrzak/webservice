import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import React, { SyntheticEvent, useEffect, useState } from 'react'
import Select from 'react-select';

const CreateInvoice = () => {
    interface ContractorDetails {
        ID: number,
        Name: string,
        Surname: string,
    }

    interface optionDetails {
        value: ContractorDetails,
        label: string,
    }

    interface productDetails {
        id: number,
        deliverID: number,
        name: string,
        description: string,
        price: string,
    }

    const [option, setOption] = useState<optionDetails[]>([]);
    const [products, setProducts] = useState<productDetails[]>([]);
    let [selectedProducts] = useState<productDetails[]>([]);
    let [selectedContractor, setSelectedContractor] = useState<optionDetails>();
    let [productsID, setProductsID] = useState<number[]>([]);

    const [buyerName, setBuyerName] = useState('');
    const [sellerName, setSellerName] = useState('');
    const [comments, setComments] = useState('');
    const [bankName, setBankName] = useState('');
    const [bankAccountNumber, setBankAccountNumber] = useState('');

    const getContractors = async () => {    
        try {
            const res = await axios.get('http://localhost:8080/get-contractors')
            
            let tmp = [];
           
            for (let i = 0; i< res.data.fakturomaniaContractors.length; i++) {
                tmp.push(
                    {
                        value: res.data.fakturomaniaContractors[i], 
                        label: res.data.fakturomaniaContractors[i].Name + " " + 
                        res.data.fakturomaniaContractors[i].Surname
                    }
                )
            }
            setOption(tmp); 
            // console.log(tmp)
        } catch (e) {
          console.log(e);
        }
    }

    const getProducts = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-products');

            console.log(res.data.products);
            let tmp = [];
            
            for (let i = 0; i < res.data.products.length; i++) {
                tmp.push(res.data.products[i]);
            }
            setProducts(tmp);
        } catch (e) {
            console.log(e);
        }
        
        // try {
        //     const res = await axios.get('http://localhost:8080/get-products');

        //     let tmp = [];

        //     for (let i = 0; i < res.data.products.length; i++) {
        //         tmp.push(
        //             {
        //                 id: res.data.products[i].ID,
        //                 deliverID: res.data.products[i].DeliverID,
        //                 name: res.data.products[i].Name,
        //                 description: res.data.products[i].Description,
        //                 price: res.data.products[i].Price
        //             }
        //         )
        //     }
        //     setProducts(tmp);
        //     console.log(tmp);
        // } catch (e) {
        //     console.log(e)
        // }
    }

    const submit = (e : SyntheticEvent) => {
        e.preventDefault();

        specifyProductsID(selectedProducts);

        axios.post('http://localhost:8080/create-invoice', {
            productsID: productsID,
            contractor: selectedContractor,
            buyerName: buyerName,
            sellerName: sellerName,
            comments: comments,
            bankName: bankName,
            bankAccountNumber: bankAccountNumber,
        }).then((res) => {

        })
        .catch((e) => {

        })

    }

    const onSelect = (data : Array<productDetails>) => {
        selectedProducts.push(data[data.length - 1])
    }

    const onRemove = (data : Array<string>) => {
        // while (emails.length > 0) {
        //     emails.pop();
        // }
        
        // if (data.length !== 0) {
        //     data.forEach((email) => {
        //         emails.push(email)
        //     })
        // }
    }

    const handleChange = (option : any) => {
        setSelectedContractor(option.value)
    }

    const specifyProductsID = (selectedProducts : productDetails[]) => {
        selectedProducts.forEach(product => {
            productsID.push(product.id)
        })
    }

    useEffect(() => {
        getContractors();
        getProducts();
    }, []);

    return (
        <form onSubmit={submit}>
            <Select options={option} onChange={handleChange} placeholder="Contractor"/>
            <Multiselect 
                isObject={true}
                onRemove={onRemove}
                onSelect={onSelect}
                options={products}
                displayValue="name"
                placeholder="Products"
            />
            <div className="form-group">
                <input type="text" name="title" className="form-control" placeholder="buyer name" required
                onChange= { e => setBuyerName(e.target.value) }/>
            </div>
            <div className="form-group">
                <input type="text" name="title" className="form-control" placeholder="seller name" required
                onChange= { e => setSellerName(e.target.value) }/>
            </div>
            <div className="form-group">
                <input type="text" name="title" className="form-control" placeholder="comments" required
                onChange= { e => setComments(e.target.value) }/>
            </div>
            <div className="form-group">
                <input type="text" name="title" className="form-control" placeholder="bank name" required
                onChange= { e => setBankName(e.target.value) }/>
            </div>
            <div className="form-group">
                <input type="text" name="title" className="form-control" placeholder="bank account number" required
                onChange= { e => setBankAccountNumber(e.target.value) }/>
            </div>
            <div className="form-group">
                <button className="w-100 btn btn-lg btn-primary" type="submit">Wystaw fakturę</button>
            </div>
        </form>
    );
};

export default CreateInvoice;