import axios from 'axios';
import React, { SyntheticEvent, useState } from 'react'
import { useCookies } from 'react-cookie';

const CreateContractor = () => {
    const [name, setName] = useState('');
    const [nip, setNip] = useState('');
    const [street, setStreet] = useState('');
    const [postalCode, setPostalCode] = useState('');
    const [postalCity, setPostalCity] = useState('');
    const [jwtCookie, setJwtCookie] = useCookies(['jwt'])

    const submit = (e : SyntheticEvent) => {
        e.preventDefault();

        axios.post('http://localhost:8080/create-contractor', {
            name: name,
            nip: nip,
            street: street,
            postalCode: postalCode,
            postalCity: postalCity,
        }, {
            headers: {
                "Authorization": "Bearer " + jwtCookie.jwt,
            }
        }).then((response) => {
            
        })
        .catch((error) => {

        })
    }

    return (
        <div>
            <form onSubmit={submit}>
                <h1 className="h3 mb-3 fw-normal">Put contractor data</h1>
                <input type="text" className="form-control" placeholder="name" required
                onChange={e => setName(e.target.value)}
                />
                
                <input type="text" className="form-control" placeholder="nip" required
                onChange={e => setNip(e.target.value)}
                />

                <input type="text" className="form-control" placeholder="street" required
                onChange={e => setStreet(e.target.value)}
                />

                <input type="text" className="form-control" placeholder="postal code" required
                onChange={e => setPostalCode(e.target.value)}
                />

                <input type="text" className="form-control" placeholder="postal city" required
                onChange={e => setPostalCity(e.target.value)}
                />

                <button className="w-100 btn btn-lg btn-primary" type="submit">Utwórz kontraktora</button>
            </form>
        </div>
    );
};

export default CreateContractor;