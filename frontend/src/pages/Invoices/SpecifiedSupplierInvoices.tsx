import React, { SyntheticEvent, useEffect, useState } from 'react'
import axios from 'axios';
import { 
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper 
} from '@material-ui/core';
import { useCookies } from 'react-cookie';

const SpecifiedSupplierInvoices = () => {
    interface InvoiceDetails {
        id: number,
        fakturomaniaContractorId: number,
        invoiceId: number,
        documentName: string,
    }   

    interface LoggedInUserDetails {
        id: number,
        fakturomaniaContractorId: number,
        role: number,
    }

    const [invoices, setInvoices] = useState<InvoiceDetails[]>([]);
    const [loggedInUserDetails, setLoggedInUserDetails] = useState<LoggedInUserDetails>();
    const [jwtCookie, setJwtCookie] = useCookies(['jwt'])

    const getLoggedInUserDetails = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-logged-in-user-details', {
                headers: {
                    "Authorization": "Bearer " + jwtCookie.jwt,
                }
            });
            setLoggedInUserDetails(res.data.user);
        } catch (e) {
            console.log(e)
        }
    }

    const getInvoices = async () => {
        try {
            let tmpb : LoggedInUserDetails
            const res1 = await axios.get('http://localhost:8080/get-logged-in-user-details', {
                headers: {
                    "Authorization": "Bearer " + jwtCookie.jwt,
                }
            });
            tmpb = res1.data.user
            setLoggedInUserDetails(res1.data.user);

            const res = await axios.get('http://localhost:8080/get-invoices')
            
            let tmp = [];
            for (let i = 0; i < res.data.invoices.length; i++) {
                if (res.data.invoices[i].fakturomaniaContractorId == tmpb.fakturomaniaContractorId) {
                    tmp.push(res.data.invoices[i]);
                }
            }
            setInvoices(tmp);


        } catch (e) {
        console.log(e);
        }
    }   

    const downloadFakturomaniaInvoice = async (invoiceId : number) => {
        try {
            const res = await axios.get(`http://localhost:8080/get-invoice-by-id/${invoiceId}`)
            const prefbase64 = "data:application/pdf;base64," + res.data.file
            console.log(prefbase64)
            const newWindow = window.open(prefbase64, '_blank', 'noopener,noreferrer')
            if (newWindow) newWindow.opener = null

        } catch (e) {
        console.log(e);
        }
    }   

    
    useEffect (() => {
        getLoggedInUserDetails();
        getInvoices();
    }, []);


    return (
        <TableContainer component={Paper}>
                <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>Id</TableCell>
                        <TableCell>Invoice Id</TableCell>
                        <TableCell>Fakturomania Contractor Id</TableCell>
                        <TableCell>Name</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                        {invoices.map((invoice) => (
                        <TableRow key={invoice.id}>
                            <TableCell>{invoice.id}</TableCell>
                            <TableCell>{invoice.invoiceId}</TableCell>
                            <TableCell>{invoice.fakturomaniaContractorId}</TableCell>
                            <TableCell>{invoice.documentName}</TableCell>
                            <TableCell><button className="w-100 btn btn-lg btn-primary" type="submit" onClick={() => downloadFakturomaniaInvoice(invoice.invoiceId)}>Pobierz</button></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                </Table>
            </TableContainer>
    );
};

export default SpecifiedSupplierInvoices;