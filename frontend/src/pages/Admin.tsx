import axios from 'axios';
import React, { SyntheticEvent, useEffect, useState } from 'react'
import userIntreface from './../interfaces/userInterface';
import { 
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper 
} from '@material-ui/core';
import Select from 'react-select';
import ModalProfileData from './../components/ModelProfileData';

interface roleVariation {
    name: string,
    dbEquivalent: number,
}

interface optionDetails {
    value: roleVariation,
    label: string,
}

interface Tasks {
    id: number,
    task: string,
    state: boolean,
}

const Admin = () => {
    //Modal
    const [isOpen, setIsOpen] = useState(false)
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [pesel, setPesel] = useState('')
    const [address, setAddress] = useState('')
    const [position, setPosition] = useState('')
    //
    const [users, setUsers] = useState<userIntreface[]>([]);
    const [option, setOption] = useState<optionDetails[]>([]);
    const [selectedRole, setSelectedRole] = useState<number>();
    const [test, setTest] = useState<number>(1);
    //
    const [id, setId] = useState(0);
    const [task, setTask] = useState('');
    const [tasks, setTasks] = useState<Tasks[]>([]);

    //const which have to display users to print in that moment
    const [usersToDisplay, setUsersToDisplay] = useState<userIntreface[]>([]);
    const AMOUNT_USERS_ON_ONE_PAGE = 20

    const roleOptions : roleVariation[] = [
        { name: "Inactive", dbEquivalent: 0},
        { name: "Employee", dbEquivalent: 1},
        { name: "Supplier", dbEquivalent: 2},
        { name: "Manager", dbEquivalent: 3},
    ];

    const getUsers = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-users')
            let tmp = [];

            for (let i = 0; i < res.data.users.length; i++) {
                tmp.push(res.data.users[i]);
            }            
            setUsers(tmp);
            setOptions();
            
            //pierwsze będzie wylistowanie pierwszej 20
            let tmpb = []
            if (res.data.users.length < 20) {
                for (let i = 0; i < res.data.users.length; i++) {
                    tmpb.push(res.data.users[i])                    
                }
            } else {
                for (let i = 0; i < AMOUNT_USERS_ON_ONE_PAGE; i++){
                    tmpb.push(res.data.users[i])
                }
            }
            if (test === 1) {
                setUsersToDisplay(tmpb);
            }
            // setUsersToDisplay(tmpb);

        } catch (e) {
          console.log(e);
        }
    }
    
    useEffect (() => {
        getUsers();    
    }, []);

    // teraz je wyprintować trzeba 
    const setOptions = () => {
        let tmp = [];
        

        for(let i = 0; i < roleOptions.length; i++) {
            tmp.push(
                {
                    value: roleOptions[i],
                    label: roleOptions[i].name
                }
            );
        }
        setOption(tmp);
    }

    const specifyRoleByDBEquivalent = (dbEquivalent: number) : string => {
        let name = "";

        roleOptions.find((roleOption) => {
            if (roleOption.dbEquivalent === dbEquivalent) {
               name = roleOption.name;
            }
        });

        return name;
    } 

    const updateRole = (userId : number, newRole? : number) => {
        try {
            axios.patch(`http://localhost:8080/update-role/${userId}`, {
                role: newRole
            }).then((response) => {
                
            })
            .catch((e) => {

            })
        } catch (e) {
            console.log(e);
        }
    }

    const handleChange = (option : any) => {
        setSelectedRole(option.value.dbEquivalent);
    }

    const moveToNextPageOfUsers = () => {
        setTest(test+1)
        console.log("mover", test)
        let tmpb = []

        for (let i = test*AMOUNT_USERS_ON_ONE_PAGE - AMOUNT_USERS_ON_ONE_PAGE; i < test*AMOUNT_USERS_ON_ONE_PAGE; i++) {
            if (i < users.length) {
                tmpb.push(users[i]);
            } else {
                setTest(test);
            }
        }
        setUsersToDisplay(tmpb);
    }   

    const moveToPreviousPageOfUsers = () => {
        setTest(test-1)
        console.log("previouser", test)
        let tmpb = []

        for (let i = test*AMOUNT_USERS_ON_ONE_PAGE - AMOUNT_USERS_ON_ONE_PAGE; i < test*AMOUNT_USERS_ON_ONE_PAGE; i++) {
            if (i > 0) {
                if (i < users.length) {
                    tmpb.push(users[i]);
                }
            } else {
                setTest(1)
            }
        }
        setUsersToDisplay(tmpb);
    }

    const displaySpecificProfileById = async (userId : number) => {
        const res = await axios.get(`http://localhost:8080/get-specific-user-details/${userId}`);

        console.log(res.data)

        setId(userId)
        setName(res.data.profile.name.String);
        setSurname(res.data.profile.surname.String);
        setPesel(res.data.profile.pesel.String);
        setAddress(res.data.profile.address.String);
        setPosition(res.data.profile.position.String);
        setTasks(res.data.tasks)

        setIsOpen(true);
    }

    const submit = async (e : SyntheticEvent) => {
        e.preventDefault()

        await axios.post(`http://localhost:8080/add-task/${id}`, {
            task: task
        }).then((res) => {
            if (res.data.message === "SUCCESS") {
                displaySpecificProfileById(id);
            }
        }).catch((e) => {
            console.log(e)
        });
    }

    const deleteTask = async (id : number) => {
        await axios.patch(`http://localhost:8080/delete-task/${id}`).then((res) => {
            if (res.data.message === "SUCCESS") {
                displaySpecificProfileById(id);
            }
        }).catch((e) => {
            console.log(e)
        })
    }

    return (
        <div>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>id</TableCell>
                        <TableCell>email</TableCell>
                        <TableCell>role</TableCell>
                        <TableCell>role options</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {
                    /* chcemy żeby tutaj wyświetlało się 20 pozycji posortowane względem id */
                    // 
    
                    }
                        {console.log("USERS !!!!:", usersToDisplay)}
                        {usersToDisplay.map((user) => (
                        <TableRow key={user.ID}>
                            <TableCell>{user.ID}</TableCell>
                            <TableCell>{user.Email}</TableCell>
                            <TableCell>{user.Role}</TableCell>
                            <TableCell>
                                <Select 
                                    options={option} 
                                    onChange={handleChange}
                                    defaultValue={{ value: {name: specifyRoleByDBEquivalent(user.Role), dbEquivalent: user.Role}, label: specifyRoleByDBEquivalent(user.Role)}} 
                                />
                            </TableCell>
                            <TableCell><button className="w-100 btn btn-lg btn-primary" type="submit" onClick={() => updateRole(user.ID, selectedRole)}>Edytuj Role</button></TableCell>
                            <TableCell><button className="w-100 btn btn-lg btn-primary" type="submit" onClick={() => displaySpecificProfileById(user.ID)}>Wyświetl Dane</button></TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                </Table>
            </TableContainer>
            <label>
                <label>
                    <button className="w-100 btn btn-lg btn-primary" onClick={() => moveToPreviousPageOfUsers()} >{'<'}</button>
                </label>
                <label>
                    <button className="w-100 btn btn-lg btn-primary" onClick={() => moveToNextPageOfUsers()} >{'>'}</button>
                </label>
            </label>
            <div>
                <ModalProfileData 
                    open={isOpen}
                    onClose={() => setIsOpen(false)}
                    name={name}
                    surname={surname}
                    pesel={pesel}
                    address={address}
                    position={position}
                    delete={(id) => deleteTask(id)}
                    tasks={tasks}
                    setTask={(task) => setTask(task)}
                    submit={(e) => submit(e)}
                >
                    dsada
                </ModalProfileData>
            </div>
        </div>
    );
};

export default Admin;