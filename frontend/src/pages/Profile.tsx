import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router';
import { useCookies } from 'react-cookie';

interface Tasks {
    id: number,
    task: string,
    state: boolean,
}

const Profile = () => {
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [pesel, setPesel] = useState('');
    const [address, setAddress] = useState('');
    const [position, setPosition] = useState('');
    const [tasks, setTasks] = useState<Tasks[]>([]);
    //cookies
    const [jwtCookie, setJwtCookie] = useCookies(['jwt']);
    
    const [refresh, setRefresh] = useState(false);

    const getProfileDetails = async () => {
        const res = await axios.get('http://localhost:8080/get-profile-details', {
            headers: {
                "Authorization": "Bearer " + jwtCookie.jwt,
            }
        });
        
        console.log("profile !!!", res.data.profile);
        console.log("name: ss", res.data.profile.name.String)
        setName(res.data.profile.name.String);
        setSurname(res.data.profile.surname.String);
        setPesel(res.data.profile.pesel.String);
        setAddress(res.data.profile.address.String);
        setPosition(res.data.profile.position.String);
        setTasks(res.data.tasks);
    }

    useEffect(() => {
        getProfileDetails();
        setRefresh(false)
    }, [refresh])

    const history = useHistory();

    const moveToChangePasswordForm = () => {
        let path = `/change-password`;
        history.push(path);
    }

    const moveToQuestionnaireForm = () => {
        let path = `/update-questionnaire`;
        history.push(path);
    }

    const deleteTask = async (id : number) => {
        await axios.patch(`http://localhost:8080/delete-task/${id}`).then((res) => {
            if (res.data.message === "SUCCESS") {
                setRefresh(true);
            }
        }).catch((e) => {
            console.log(e)
        })
    }

    return (
        <div>
            <h3>Dane Osobowe</h3>
            <div>Imię {name ? name : "Nie ustawiono" }</div>
            <div>Nazwisko {surname ? surname : "Nie ustawiono"}</div>
            <div>Pesel {pesel ? pesel : "Nie ustawiono"}</div>
            <div>Adres {address ? address : "Nie ustawiono"}</div>
            <div>Stanowisko {position ? position : "Nie ustawiono"}</div>
            <hr></hr>
            <h3>Zadania</h3>
            { tasks !== null && (
                    tasks.map(task => (
                        (<div>{task.task}<label><button className="w-100 btn btn-lg btn-primary" onClick={() => {deleteTask(task.id)}}>x</button></label></div>)
                    ))
                )}
            <hr></hr>
            <div>
                <button id="form-update-questionnaire" className="w-100 btn btn-lg btn-primary" type="submit" onClick={moveToQuestionnaireForm}>Edytuj kwestionariusz</button>
            </div>
            <div>
                <button id="form-change-password" className="w-100 btn btn-lg btn-primary" type="submit" onClick={moveToChangePasswordForm}>Zmień hasło</button>
            </div>
        </div>
    );
};

export default Profile;