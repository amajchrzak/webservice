import axios from 'axios';
import React, { SyntheticEvent, useState } from 'react'

const ResetPasswordForm = () => {
    const [token, setToken] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [retypedNewPassword, setRetypedNewPassword] = useState('');

    const submit = async (e : SyntheticEvent) => {
        e.preventDefault();

        axios.post('http://localhost:8080/reset-password', {
            token: token,
            newPassword: newPassword,
            retypedNewPassword: retypedNewPassword,
        }).then((response) => {
            console.log(response)
        })
        .catch((error) => {
            console.log(error)
        })
    }
    
    return (
        <div>
            <form onSubmit={submit}>
                <h1 className="h3 mb-3 fw-normal">Reset password</h1>
                <input type="text" className="form-control" placeholder="Token" required
                onChange={e => setToken(e.target.value)}
                />
                
                <input type="password" className="form-control" placeholder="New password" required
                onChange={e => setNewPassword(e.target.value)}
                />

                <input type="password" className="form-control" placeholder="Retype new password" required
                onChange={e => setRetypedNewPassword(e.target.value)}
                />
                
                <button className="w-100 btn btn-lg btn-primary" type="submit">Reset password</button>
            </form>
        </div>
    );
};

export default ResetPasswordForm;