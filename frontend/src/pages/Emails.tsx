import axios from 'axios';
import Multiselect from 'multiselect-react-dropdown';
import React, { SyntheticEvent, useEffect, useState } from 'react'
import userIntreface from './../interfaces/userInterface';

const Emails = () => {
    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');
    let [emails] = useState<string[]>([]);
    const [listOfEmails] = useState<string[]>([]);
    
    const getUsers = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-users')
            const listOfUsers = res.data.users;
           
            listOfUsers.forEach((email : userIntreface) => {
                listOfEmails.push(email.Email)
            });

        } catch (e) {
          console.log(e);
        }
    }
    
    useEffect (() => {
        getUsers();        
    });
    

    const submit = async (e : SyntheticEvent) => {
        e.preventDefault()

        axios.post('http://localhost:8080/send-email', {
            title: title,
            message: message,
            emails: emails,
        }).then(() => {
            console.log(title, message, emails);
        })
        .catch(() => {

        });
    }

    const onSelect = (data : Array<string>) => {
        emails.push(data[data.length - 1])
    }

    const onRemove = (data : Array<string>) => {
        while (emails.length > 0) {
            emails.pop();
        }
        
        if (data.length !== 0) {
            data.forEach((email) => {
                emails.push(email)
            })
        }
    }

    return (
        <div>
            <form onSubmit={submit}>
                    <Multiselect 
                        isObject={false}
                        onRemove={onRemove}
                        onSelect={onSelect}
                        options={listOfEmails}
                        displayValue="emails"
                    />
                    <div className="form-group">
                        <input type="text" name="title" className="form-control" placeholder="Title" required
                        onChange= { e => setTitle(e.target.value) }/>
                    </div>
                    <div className="form-group">
                        <textarea name="message" id="message" className="form-control" rows={4} placeholder="Enter your message" required
                        onChange= { e => setMessage(e.target.value) }></textarea>
                    </div>
                    <div className="form-group">
                        <button className="w-100 btn btn-lg btn-primary" type="submit">Wyślij Email</button>
                    </div>
                    
            </form>
        </div>
    );
};

export default Emails;