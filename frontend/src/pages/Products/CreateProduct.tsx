import axios from 'axios';
import React, { SyntheticEvent, useState, useEffect } from 'react'
import Select from 'react-select';

const CreateProduct = () => {
    interface Deliver {
        id: number
        name: string
    }

    interface OptionDetails {
        value: Deliver,
        label: string
    }

    const [productName, setProductName] = useState('');
    const [productDescription, setProductDescription] = useState('');
    const [productPrice, setProductPrice] = useState('');
    //wyciągnąć deliverowców i przypisać dany produkt do deliverowca
    const [delivers, setDelivers] = useState<Deliver[]>([]);
    const [option, setOption] = useState<OptionDetails[]>();
    const [selectedDeliver, setSelectedDeliver] = useState<OptionDetails>();
    const [deliverId, setDeliverId] = useState<number>(0);

    const getDelivers = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-delivers')
            let tmp = [];
            
            console.log(res.data.delivers);

            for (let i = 0; i < res.data.delivers.length; i++) {
                tmp.push(
                    {
                        value: res.data.delivers[i],
                        label: res.data.delivers[i].name
                    }
                );
            }
            setOption(tmp);
        } catch (e) {
            console.log(e);
        }
    }

    const handleChange = (option : any) => {
        setSelectedDeliver(option.value)
        setDeliverId(option.value.id);
    }

    useEffect (() => {
        getDelivers();
    }, []);

    const submit = (e : SyntheticEvent) => {
        e.preventDefault()
        
        axios.post('http://localhost:8080/add-product', {
            name: productName,
            description: productDescription,
            price: productPrice,
            deliverId: deliverId, 
        }).then((response) => {
            
        })
        .catch((error) => {

        })
    }

    return (
        <div>
            <form onSubmit={submit}>
                <h1 className="h3 mb-3 fw-normal">Put product data</h1>
                <Select options={option} onChange={handleChange} placeholder="Contractor"/>
                
                <input type="text" className="form-control" placeholder="product name" required
                onChange={e => setProductName(e.target.value)}
                />
                
                <input type="text" className="form-control" placeholder="product description" required
                onChange={e => setProductDescription(e.target.value)}
                />

                <input type="text" className="form-control" placeholder="product price" required
                onChange={e => setProductPrice(e.target.value)}
                />

                <button className="w-100 btn btn-lg btn-primary" type="submit">Dodaj Produkt</button>
            </form>
        </div>
    );
};

export default CreateProduct;