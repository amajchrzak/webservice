import React from 'react'
import { useHistory } from 'react-router';

const Invoices = () => {

    const history = useHistory();

    const redirectToCreateContractorForm = () => {
        let path = `create-contractor`
        history.push(path)
    }

    const redirectToCreateInvoiceForm = () => {
        let path = `create-invoice`
        history.push(path)
    }

    const redirectToListInvoicesPage = () => {
        let path = `get-invoices`
        history.push(path)
    }

    const redirectToListInvoicesSpecifiedIssuedByLoggedSupplier = () => {
        let path = `get-invoices-issued-by-logged-supplier`
        history.push(path)
    }

    return (
        <div>
            <div>
                <button className="w-100 btn btn-lg btn-primary" type="submit" onClick={redirectToCreateContractorForm}>Dodaj kontraktora</button>        
            </div>
            <div>
                <button className="w-100 btn btn-lg btn-primary" type="submit" onClick={redirectToCreateInvoiceForm}>Utwórz fakturę</button>
            </div>
            <div>
                <button className="w-100 btn btn-lg btn-primary" type="submit" onClick={redirectToListInvoicesPage}>Wyświetl faktury</button>
            </div>
            <div>
                <button className="w-100 btn btn-lg btn-primary" type="submit" onClick={redirectToListInvoicesSpecifiedIssuedByLoggedSupplier}>Wyświetl moje faktury</button>
            </div>
        </div>
    );
};

export default Invoices;