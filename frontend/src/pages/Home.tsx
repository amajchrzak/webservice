import React from 'react'

const Home = (props: { name: string, role: number }) => {
    return (
        <div>
            {props.name ? 'Cześć ' + props.name + ' ' + props.role : 'Nie jesteś zalogowany! '}
        </div>
    );
};

export default Home;