import axios from 'axios';
import React, { SyntheticEvent, useState, useEffect } from 'react'
import { Redirect } from 'react-router-dom';
import { useCookies } from 'react-cookie';

interface LoggedInUserDetails {
    id: number,
    fakturomaniaContractorId: number,
    role: number,
}

const ChangePassword = () => {
    const [password, setPassword] = useState('')
    const [newPassword, setNewPassword] = useState('')
    const [retypedNewPassword, setRetypedNewPassword] = useState('')
    const [redirect, setRedirect] = useState(false);
    const [loggedInUserDetails, setLoggedInUserDetails] = useState<LoggedInUserDetails>(
        {
            id: 0, 
            fakturomaniaContractorId: 0, 
            role: 0
        }
    );
    const [jwtCookie, setJwtCookie] = useCookies(['jwt']);
    const [id, setId] = useState(0);

    const submit = (e: SyntheticEvent) => {
        e.preventDefault()

        axios.post(`http://localhost:8080/change-password/${id}`, {
            password: password,
            newPassword: newPassword,
            retypedNewPassword: retypedNewPassword,
        }).then((response) => {
            console.log(response)
            if (response.data.message === "SUCCESS") {
                setRedirect(true);
            }
        })
        .catch((error) => {
            console.log(error)
        })
    }

    const getLoggedInUserDetails = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-logged-in-user-details', {
                headers: {
                    "Authorization": "Bearer " + jwtCookie.jwt,
                }
            });
            setLoggedInUserDetails(res.data.user);
            setId(res.data.user.id);
        } catch (e) {
            console.log(e)
        }
    }

    useEffect (() => {
        getLoggedInUserDetails();
    }, []);

    if(redirect) {

        return <Redirect to="/profile" />;
    }

    return (
        <form onSubmit={submit}>
            <h1 className="h3 mb-3 fw-normal">Change password</h1>
            <input type="password" id="change-password-password" className="form-control" placeholder="Password" required
            onChange={e => setPassword(e.target.value)}
            />
            
            <input type="password" id="change-password-new-password" className="form-control" placeholder="New password" required
            onChange={e => setNewPassword(e.target.value)}
            />

            <input type="password" id="change-password-retyped-new-password" className="form-control" placeholder="Retype new password" required
            onChange={e => setRetypedNewPassword(e.target.value)}
            />
            
            <button id="button-change-password-submit" className="w-100 btn btn-lg btn-primary" type="submit">Zmień hasło</button>
        </form>
    );
};

export default ChangePassword;