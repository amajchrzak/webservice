import React, { useState, useEffect, SyntheticEvent } from 'react'
import FullCalendar, { EventInput } from '@fullcalendar/react';
import dayGridPlugin from '@fullcalendar/daygrid';
import timeGridPlugin from '@fullcalendar/timegrid';
import interactionPlugin from '@fullcalendar/interaction';
import axios from 'axios';
import moment from 'moment';
import Modal from './Modal';
import CSS from 'csstype';
import { useCookies } from 'react-cookie';

interface EmployeesScheduleDetails {
    id: number,
    userId: number,
    name: string,
    startDayOfWork: string,
    endDayOfWork: string,
    startDate: string
}

interface employeeSelect {
    userId: number,
    name: string
}

interface employeesOptions{
    value: employeeSelect
    label: string
}

interface LoggedInUserDetails {
    id: number,
    fakturomaniaContractorId: number,
    role: number,
}

const Calendar = () => {
    let calendarRender = (
        <div> </div>
    )

    const [isOpen, setIsOpen] = useState(false)
    const [employeeName, setEmployeeName] = useState('')
    const [employeesOptions, setEmployeesOptions] = useState<employeesOptions[]>([]);
    const [employeeOption, setEmployeeOption] = useState<any>();
    const [dateStr, setDateStr] = useState('');
    const [startDayOfWork, setStartDayOfWork] = useState('');
    const [endDayOfWork, setEndDayOfWork] = useState('');

    const [employees, setEmployees] = useState<EmployeesScheduleDetails[]>([])
    const [events, setEvents] = useState<EventInput[]>([])
   
    //logged in user
    const [loggedInUserDetails, setLoggedInUserDetails] = useState<LoggedInUserDetails>(
        {
            id: 0, 
            fakturomaniaContractorId: 0, 
            role: 0
        }
    );
    const [jwtCookie, setJwtCookie] = useCookies(['jwt'])

    const getLoggedInUserDetails = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-logged-in-user-details', {
                headers: {
                    "Authorization": "Bearer " + jwtCookie.jwt,
                }
            });
            setLoggedInUserDetails(res.data.user);
        } catch (e) {
            console.log(e)
        }
    }


    const getEmployeesScheduleInfo = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-employees-schedule')
            
            let tmp = []
            console.log( res.data)
            for (let i = 0; i < res.data.employeesScheduleData.length; i++) {
                tmp.push(res.data.employeesScheduleData[i]); 
            }
            setEmployees(tmp);
            
            let tmpa = []    
            console.log(tmp.length)
            for (let i = 0; i < tmp.length; i++) {
                const prepareTitle = tmp[i].name + " " + tmp[i].startDayOfWork + "-" + tmp[i].endDayOfWork
                let formattedDate = moment(tmp[i].start).format('YYYY-MM-DD') 
                tmpa.push({id: tmp[i].id.toString(), title: prepareTitle , start: formattedDate})
                console.log("date: ", formattedDate)
            }
            setEvents(tmpa)
        } catch (e) {
            console.log(e)
        }
    }


    const getSpecificBillingAddressData = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-billing-addresses')

            let tmp = [];
            for (let i = 0; i < res.data.billingAddress.length; i++) {
                tmp.push(
                    {
                        value: res.data.billingAddress[i], 
                        label: res.data.billingAddress[i].name 
                    }
                );
            }            
            setEmployeesOptions(tmp);
        } catch (e) {
            console.log(e);
        }
    }
    
    const submit = async (e : SyntheticEvent) => {
        e.preventDefault();

        await axios.post('http://localhost:8080/insert-user-to-work-schedule', {
            userId: employeeOption.userId,
            name: employeeOption.name,
            start: dateStr,
            end: dateStr,
            startDayOfWork: startDayOfWork,
            endDayOfWork: endDayOfWork
        }).then(() => {
            
        }).catch((e) => {
            console.log(e)
        })
        
        setIsOpen(false);
        getEmployeesScheduleInfo();
    }

    useEffect (() => {
        getEmployeesScheduleInfo();
        getSpecificBillingAddressData();
        getLoggedInUserDetails();
    }, []);

    if (loggedInUserDetails.role === 3) {
        calendarRender = (
            <div>
                <div>
                    <FullCalendar
                        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                        initialView="dayGridMonth"
                        events={events}
                        eventColor="light-blue"
                        selectable={true}
                        select={(e) => {
                            setIsOpen(true)
                        }}
                        nowIndicator
                        dateClick={(e) => setDateStr(e.dateStr)}
                    />
                </div>
                <div>
                    <Modal 
                        open={isOpen}
                        options={employeesOptions} 
                        onClose={() => setIsOpen(false)}
                        sub={(e) => submit(e)} 
                        setEmployeeName={(employeeName) => setEmployeeName(employeeName)}
                        handleChange={(employeeOption) => setEmployeeOption(employeeOption)}
                        setStartDayOfWork={(startDayOfWork) => setStartDayOfWork(startDayOfWork)}
                        setEndDayOfWork={(endDayOfWork) => setEndDayOfWork(endDayOfWork)}
                    >
                        dasda
                    </Modal>
                </div>
            </div>    
        )
    } else {
        calendarRender = ( 
            <div>
                <div>
                    <FullCalendar
                        plugins={[dayGridPlugin, timeGridPlugin, interactionPlugin]}
                        initialView="dayGridMonth"
                        events={events}
                        eventColor="light-blue"
                        selectable={true}
                        select={(e) => {
                            setIsOpen(true)
                        }}
                        nowIndicator
                        dateClick={(e) => setDateStr(e.dateStr)}
                    />
                </div>
            </div>
        )
    }

    return calendarRender
};


export default Calendar;
