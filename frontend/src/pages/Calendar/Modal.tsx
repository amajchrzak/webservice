import React, { SyntheticEvent } from 'react'
import CSS from 'csstype';
import Select from 'react-select';


const MODAL_STYLES: CSS.Properties = {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: '#FFF',
    padding: '50px',
    zIndex: 1000
}

const OVERLAY_STYLES: CSS.Properties = {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    background: 'rgb(0,0,0, .7)',
    zIndex: 1000
}

interface employeeSelect {
    userId: number,
    name: string
}

interface employeesOptions{
    value: employeeSelect
    label: string
}

export default function Modal(props: 
    { 
    open: boolean, 
    children : string, 
    options: employeesOptions[],
    onClose: ()=>void,
    sub: (e: SyntheticEvent)=>void, 
    setEmployeeName: (employeeName: string) => void 
    setStartDayOfWork: (startOfWorkDate: string) => void
    setEndDayOfWork: (endOfWorkDate: string) => void
    handleChange: (employeeOption: any) => void
}) {
    if(!props.open) return null
    return (
        <>
            <div style={OVERLAY_STYLES} />
            <div style={MODAL_STYLES} >
                { <Select options={props.options} onChange={e => props.handleChange(e?.value)} placeholder="Employee"></Select> }
                
                Godzina Rozpoczęcia<input type="time" className="form-control" onChange={e => props.setStartDayOfWork(e.target.value)} />
                Godzina Zakończenia<input type="time" className="form-control" onChange={e => props.setEndDayOfWork(e.target.value)} /> 
                
                <button className="w-100 btn btn-lg btn-primary" onClick={props.sub}>Zapisz zmiany</button>
                <button className="w-100 btn btn-lg btn-primary" onClick={props.onClose}>Zamknij</button>
            </div>
        </>
    )
};