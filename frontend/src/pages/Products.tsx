import axios from 'axios';
import React, { useEffect, useState } from 'react'
import { useHistory } from 'react-router';
import { 
    Table,
    TableBody,
    TableCell,
    TableContainer,
    TableHead,
    TableRow,
    Paper 
} from '@material-ui/core';
import { useCookies } from 'react-cookie';

interface productDetails {
    id: number
    deliverId: number
    name: string
    description: string
    price: string
}

interface deliverName {
    id: number
    name: string
}

interface LoggedInUserDetails {
    id: number,
    fakturomaniaContractorId: number,
    role: number,
}

const Products = () => {
   // let displayPattern = (<div></div>)

    const [products, setProducts] = useState<productDetails[]>([]);
    //const [productsAssignedToSupplier, setProductsAssignedToSupplier] = useState<productDetails[]>([]);
    const [delivers, setDelivers] = useState<deliverName[]>([]);
    // const [loggedInUserDetails, setLoggedInUserDetails] = useState<LoggedInUserDetails>(
    //     {
    //         id: 0, 
    //         fakturomaniaContractorId: 0, 
    //         role: 0
    //     }
    // );
    // const [jwtCookie, setJwtCookie] = useCookies(['jwt'])

    // const getLoggedInUserDetails = async () => {
    //     try {
    //         const res = await axios.get('http://localhost:8080/get-logged-in-user-details', {
    //             headers: {
    //                 "Authorization": "Bearer " + jwtCookie.jwt,
    //             }
    //         });
    //         setLoggedInUserDetails(res.data.user);
    //     } catch (e) {
    //         console.log(e)
    //     }
    // }

    const getProducts = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-products');

            console.log(res.data.products);
            let tmp = [];
            
            for (let i = 0; i < res.data.products.length; i++) {
                tmp.push(res.data.products[i]);
            }
            setProducts(tmp);
        } catch (e) {
            console.log(e);
        }
    }   

    const getDelivers = async () => {
        try {
            const res = await axios.get('http://localhost:8080/get-delivers');

            let tmp = [];
            
            for (let i = 0; i < res.data.delivers.length; i++) {
                tmp.push(res.data.delivers[i]);
            }
            setDelivers(tmp);
        } catch (e) {
            console.log(e);
        }
    }

    useEffect(() => {
        getProducts();
        getDelivers();
        // getLoggedInUserDetails();
    }, [])

    // const setProductsWhichShouldBeDisplayToSupplier = () => {
    //     if (loggedInUserDetails.role === 2) {
    //         let tmp = []

    //         for (let i = 0; i < products.length; i++) {
    //             if (loggedInUserDetails.deliverId === delivers[])
    //             tmp.push(products[i]);
    //         }

    //         setProductsAssignedToSupplier(tmp)
    //     }
    // }

    const history = useHistory();

    const redirectToCreateProductForm = () => {
        let path = `create-product`
        history.push(path)
    }

    const specifyDeliverNameByID = (productID: number) => {
        let name;

        delivers.find((deliver) => {
            if (deliver.id === productID) {
               name = deliver.name;
            }
        });

        return name;
    } 



    return (
        <div>
            <TableContainer component={Paper}>
                <Table aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell>id</TableCell>
                        <TableCell>deliver name</TableCell>
                        <TableCell>name</TableCell>
                        <TableCell>description</TableCell>
                        <TableCell>price</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                        {products.map((product) => (
                        <TableRow key={product.id}>
                            <TableCell>{product.id}</TableCell>
                            <TableCell>{specifyDeliverNameByID(product.deliverId)}</TableCell>
                            <TableCell>{product.name}</TableCell>
                            <TableCell>{product.description}</TableCell>
                            <TableCell>{product.price}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
                </Table>
            </TableContainer>
            <div>
                <button className="w-100 btn btn-lg btn-primary" type="submit" onClick={redirectToCreateProductForm}>Dodaj nowy produkt</button>        
            </div>
        </div>
    );
};

export default Products;