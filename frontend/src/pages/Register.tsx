import axios from 'axios';
import React, { SyntheticEvent, useState } from 'react'
import { Redirect } from 'react-router-dom';

const Register = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [retypedPassword, setRetypedPassword] = useState('');
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [redirect, setRedirect] = useState(false);

    const submit = async (e : SyntheticEvent) => {
        e.preventDefault()
        
        axios.post('http://localhost:8080/register', {
          email: email,
          password: password,
          retypedPassword: retypedPassword,
          name: name,
          surname: surname,
        }).then((res) => {
          if (res.status === 200) {
            setRedirect(true)
          }
        }).catch((e) => {
          console.log(e)
        })
    }
    
    if (redirect) {
        return <Redirect to="/login/" />;
    }

    return (
        <form onSubmit={submit}>
          <h1 className="h3 mb-3 fw-normal">Proszę zarejestruj się</h1>
          <input type="email" className="form-control" placeholder="Email" required
            onChange={e => setEmail(e.target.value)}
          />
          
          <input type="password" className="form-control" placeholder="Password" required
            onChange={e => setPassword(e.target.value)}
          />

          <input type="password" className="form-control" placeholder="Retype password" required
            onChange={e => setRetypedPassword(e.target.value)}
          />

          <input type="text" className="form-control" placeholder="Name" required
            onChange={e => setName(e.target.value)}
          />
          
          <input type="text" className="form-control" placeholder="Surname" required
            onChange={e => setSurname(e.target.value)}
          />

          <button className="w-100 btn btn-lg btn-primary" type="submit">Zarejestruj</button>
        </form>
    );
};

export default Register;