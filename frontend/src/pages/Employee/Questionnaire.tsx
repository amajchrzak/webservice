import axios from 'axios';
import React, { SyntheticEvent, useState } from 'react'
import { useCookies } from 'react-cookie';
import { Redirect } from 'react-router-dom';

const Questionnaire = () => {
    const [name, setName] = useState('')
    const [surname, setSurname] = useState('')
    const [pesel, setPesel] = useState('')
    const [address, setAddress] = useState('')
    const [position, setPosition] = useState('')
    //cookies
    const [jwtCookie, setJwtCookie] = useCookies(['jwt'])
    
    const [redirect, setRedirect] = useState(false)

    const submit = (e: SyntheticEvent) => {
        e.preventDefault()

        axios.patch('http://localhost:8080/update-questionnaire', {
            name: name,
            surname: surname,
            pesel: pesel,
            address: address,
            position: position,
        }, {
            headers: {
                "Authorization": "Bearer " + jwtCookie.jwt,
            }
        }).then((response) => {
            console.log(response)

            setRedirect(true)
        })
        .catch((error) => {
            console.log(error)
        })
    }

    if(redirect) {

        return <Redirect to="/profile" />;
    }

    return (
        <form onSubmit={submit}>
            <h1 className="h3 mb-3 fw-normal">Questionnaire</h1>
            <input id="questionnaire-name" type="text" className="form-control" placeholder="name" required
            onChange={e => setName(e.target.value)}
            />
            
            <input id="questionnaire-surname" type="text" className="form-control" placeholder="surname" required
            onChange={e => setSurname(e.target.value)}
            />

            <input id="questionnaire-pesel" type="text" className="form-control" placeholder="pesel" required
            onChange={e => setPesel(e.target.value)}
            />

            <input id="questionnaire-address" type="text" className="form-control" placeholder="address" required
            onChange={e => setAddress(e.target.value)}
            />

            <input id="questionnaire-position" type="text" className="form-control" placeholder="position" required
            onChange={e => setPosition(e.target.value)}
            />
            
            <button className="w-100 btn btn-lg btn-primary" type="submit">Edytuj kwestionariusz</button>
        </form>
    );
};

export default Questionnaire;