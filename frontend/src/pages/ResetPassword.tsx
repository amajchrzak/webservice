import axios from 'axios';
import React, { SyntheticEvent, useState } from 'react'
import { Redirect } from 'react-router';

const ResetPassword = () => {
    const [email, setEmail] = useState('');
    let [redirect, setRedirect] = useState(false);

    const submit = async (e : SyntheticEvent) => {
        e.preventDefault();

        axios.post('http://localhost:8080/send-reset-password-procedure', {
            email: email,
        }).then((response) => {
            if (response.data.message === "SUCCESS" && response.status === 200) {
                console.log('Wiadomość z tokenem została wysłana');

                setRedirect(true);
            }
        })
        .catch((error) => {
            console.log(error);
        })
    };

    if (redirect) {
        return <Redirect to="/reset-password-form"/>;
    }

    return (
        <form onSubmit={submit}>
            <h1 className="h3 mb-3 fw-normal">Podaj email</h1>
            <input type="email" className="form-control" placeholder="Email" required
            onChange={e => setEmail(e.target.value)}
            />
            
            <button className="w-100 btn btn-lg btn-primary" type="submit">Prześlij</button>
        </form>
    );
};

export default ResetPassword;