import axios from 'axios';
import React, { useEffect, SyntheticEvent, useState } from 'react'
import { Redirect, Link } from 'react-router-dom';
import Cookie from 'js-cookie'

const Login = (props: { setName: (name: string) => void, setRole: (role: number) => void }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [redirect, setRedirect] = useState(false);


  const submit = async (e: SyntheticEvent) => {
    e.preventDefault();  
    
    await axios.post('http://localhost:8080/login', {
      email: email,
      password: password
    }, {withCredentials:true}).then(res => {
      console.log('haj')
      console.log(res.data)
      if(res.status === 200) {
        setRedirect(true);
        props.setRole(res.data.role)
        props.setName(res.data.email)
        console.log("submit", res.data.email, res.data.role)
        // console.log("login submit async", props.name, role)
      } else {
        console.log('else')
      }
    }).catch((e) => {
      console.log(e.response)
    })
  };
  


  if(redirect) {
    console.log(redirect);
    return <Redirect to="/" />;
  }

  return (
      <div>
        <form onSubmit={submit}>
          <h1 className="h3 mb-3 fw-normal">Proszę się zalogować</h1>
          
          <input type="email" className="form-control" placeholder="name@example.com" required
            onChange={e => setEmail(e.target.value)}
          />
          
          <input type="password" className="form-control" placeholder="Password" required
            onChange={e => setPassword(e.target.value)}
          />
          {console.log("tu jestem...")}
          <button id="login-submit" className="w-100 btn btn-lg btn-primary" type="submit">Zaloguj</button>
        </form>
          <div>
            <Link to="reset-password" className="nav-link active">Zapomniałeś hasła?</Link>          
          </div>
        </div>
    );
};

export default Login;