import React, { useState, useEffect } from 'react';
// import logo from './logo.svg';
import './App.css';
import { Switch, BrowserRouter, Route } from "react-router-dom";
import axios from 'axios';

import Nav from './components/Nav';

import Login from './pages/Login';
import Home from './pages/Home';
import Register from './pages/Register';
import Emails from './pages/Emails';
import Profile from './pages/Profile';
import ChangePassword from './pages/ChangePassword';
import ResetPassword from './pages/ResetPassword';
import ResetPasswordForm from './pages/ResetPasswordForm';
import Invoices from './pages/Invoices';
import CreateContractor from './pages/Invoices/CreateContractor';
import CreateInvoice from './pages/Invoices/CreateInvoice';
import GetInvoices from './pages/Invoices/GetInvoices';
import CreateProduct from './pages/Products/CreateProduct';
import Products from './pages/Products';
import Admin from './pages/Admin';
import Calendar from './pages/Calendar/Calendar';
import SpecifiedSupplierInvoices from './pages/Invoices/SpecifiedSupplierInvoices';
import Questionnaire from './pages/Employee/Questionnaire';
import { useCookies } from 'react-cookie';

function App() {
  const [name, setName] = useState('');
  const [role, setRole] = useState(0);
  const [jwtCookie, setJwtCookie] = useCookies(['jwt'])
  
  const sendGetRequest = async () => {
    try {
      const res = await axios.get('http://localhost:8080/get-logged-in-user-details', {
          headers: {
              "Authorization": "Bearer " + jwtCookie.jwt,
          }
      });
      
      if (res.status == 200) {
        setName(res.data.user.email);
        setRole(res.data.user.role);
      } else if (res.status == 401) {
        setName('');
        setRole(0);
      }
    } catch (e) {
      console.log(e);
    }
  }

  useEffect(() => {
    sendGetRequest();
  }, []);

  return (
    <div className="App">
        <BrowserRouter>
          <Nav name={name} setName={setName} role={role} setRole={setRole}/>
          <Switch>
            <>
          <main className="form-signin">
             { /* 
              supplier 
              
              todo:
              // --------------------------------------------------------------------------
              - supplier has to have possibility to display list of his products.
              - create questionnaire to add supplier -> 
            */ }
            
              {role === 0 && (
                <Route path="/" exact component={() => <Home name={name} role={role}/>} />        
              )}
              {role === 0 && (
                <Route path="/login" component={() => <Login setName={setName} setRole={setRole} />} />
              )}
              {role === 0 && (
                <Route path="/register" component={Register} />
              )}
              {role === 0 && (
                <Route path="/reset-password" component={ResetPassword} /> 
              )}
              {role === 0 && (
                <Route path="/reset-password-form" component={ResetPasswordForm} />
              )}
              {role === 3 && (
                <Route path="/create-product" component={CreateProduct} /> 
              )}
              { (role === 3 || role === 2) && (
                <Route path="/invoices" component={Invoices} />
              )}
              { (role === 3 || role === 2) && (
                <Route path="/create-invoice" component={CreateInvoice} />  
              )}
              { (role === 3 || role === 2) && (
                <Route path="/create-contractor" component={CreateContractor} />
              )}
              { (role === 3 || role === 2 || role === 1) && (
                <Route path="/update-questionnaire" component={Questionnaire} />
              )}
              { (role === 3 || role === 2 || role === 1) && (
                <Route path="/change-password" component={ChangePassword} />  
              )}
              { (role === 3 || role === 2 || role === 1) && (
                <Route path="/profile" component={Profile}/>
              )} 
            
              
          </main>
              {role === 2 && (
                <Route path="/get-invoices-issued-by-logged-supplier" component={SpecifiedSupplierInvoices} />
              )}
              {role === 3 && (
                <Route path="/products" component={Products} />
              )}
              { role === 3 && (
                <Route path="/admin-management-panel" component={Admin} />
              )}
              { role === 3 && (
                    <Route path="/emails" component={Emails} />
              )}
              {role === 3 && (
                <Route path="/get-invoices" component={GetInvoices} />
              )}
              { (role === 3 || role === 1) && (
                <Route path="/calendar" component={Calendar} />
              )}
              </>
          </Switch> 
        </BrowserRouter>
    </div>
  );
}

export default App;
