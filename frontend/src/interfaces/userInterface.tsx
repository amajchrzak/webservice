export default interface userDetails {
    ID: number,
    Email: string,
    Role: number,
}