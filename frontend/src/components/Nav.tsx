import axios from 'axios';
import React, { SyntheticEvent, useEffect, useState } from 'react';
import { Redirect, Link } from 'react-router-dom';

const Nav = (props: { name: string, setName: (name: string) => void, role: number, setRole: (role: number) => void }) => {
    let menu
    const [redirect, setRedirect] = useState(false)

    const logout = async (e : SyntheticEvent) => {
        e.preventDefault();

        await axios.post('http://localhost:8080/logout', undefined, {withCredentials:true}).then((res) => {
            if (res.status === 200) {
                props.setName('');
                props.setRole(0);
                setRedirect(true)
            }
        }).catch((e) => {
            console.log(e)
        })
    }

    if (redirect) {
        <Redirect to="/" />
    }
    
    console.log(props.name, props.role)

    if (props.name === '' && props.role === 0) {
        menu = (
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                    <Link to="login" className="nav-link active">Zaloguj</Link>
                </li>
                <li className="nav-item">
                    <Link to="register" className="nav-link active" >Zarejestruj</Link>
                </li>
            </ul>
        )
    } else if (props.name !== '' && props.role === 3) {
        menu = (
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                    <Link to="invoices" className="nav-link active">Faktury</Link>
                </li>
                <li className="nav-item">
                    <Link to="products" className="nav-link active">Produkty</Link>
                </li>
                <li className="nav-item">
                    <Link to="calendar" className="nav-link active">Kalendarz</Link>
                </li>
                <li className="nav-item">
                    <Link to="admin-management-panel" className="nav-link active">Admin</Link>
                </li>
                <li className="nav-item">
                    <Link id="profile-button" to="profile" className="nav-link active" >Profil</Link>
                </li>
                <li className="nav-item">
                    <Link to="emails" className="nav-link active" >Emaile</Link>
                </li>
                <li className="nav-item">
                    <Link to="login" className="nav-link active" onClick={logout}>Wyloguj</Link>
                </li>
            </ul>
        )
    } else if (props.name !== '' && props.role === 2) {
        menu = (
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                    <Link to="invoices" className="nav-link active">Invoices</Link>
                </li>
                <li className="nav-item">
                    <Link to="profile" className="nav-link active" >Profile</Link>
                </li>
                <li className="nav-item">
                    <Link to="login" className="nav-link active" onClick={logout}>Logout</Link>
                </li>
            </ul>
        )
    } else if (props.name !== '' && props.role === 1) {
        menu = (
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                    <Link to="calendar" className="nav-link active">Calendar</Link>
                </li>
                <li className="nav-item">
                    <Link to="profile" className="nav-link active" >Profile</Link>
                </li>
                <li className="nav-item">
                    <Link to="login" className="nav-link active" onClick={logout}>Logout</Link>
                </li>
            </ul>
        )
    } else {
        menu = (
            <ul className="navbar-nav me-auto mb-2 mb-md-0">
                <li className="nav-item">
                    <Link to="login" className="nav-link active">Login</Link>
                </li>
                <li className="nav-item">
                    <Link to="register" className="nav-link active" >Register</Link>
                </li>
            </ul>
        )
    }

    return (
        <nav className="navbar navbar-expand-md navbar-dark bg-dark mb-4">
            <div className="container-fluid">
            <Link to="/" className="navbar-brand">Home</Link>
            <div>
                {menu}
            </div>
            </div>
        </nav>
    );
};

export default Nav;
