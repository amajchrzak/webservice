import React, { SyntheticEvent } from 'react'
import CSS from 'csstype';
import ReactDOM from 'react-dom';


const MODAL_STYLES: CSS.Properties = {
    position: 'fixed',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    backgroundColor: '#FFF',
    padding: '50px',
    zIndex: 1000
}

const OVERLAY_STYLES: CSS.Properties = {
    position: 'fixed',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    background: 'rgb(0,0,0, .7)',
    zIndex: 1000
}

interface Tasks {
    id: number,
    task: string,
    state: boolean,
}

export default function ModalProfileData(props: 
    { 
    open: boolean,
    children : string,
    onClose: ()=>void,
    name: string,
    surname: string,
    pesel: string,
    address: string,
    position: string,
    tasks: Array<Tasks>,
    delete: ((id: number) => void),
    setTask: (task: string) => void
    submit: (e: SyntheticEvent) => void 
}) {
    if(!props.open) return null
    return (
        <>
            <div style={OVERLAY_STYLES} />
            <div style={MODAL_STYLES} > 
                <div>Imię {props.name ? props.name : "Brak danych"}</div>
                <div>Nazwisko {props.surname ? props.surname : "Brak danych"}</div>
                <div>Pesel {props.pesel ? props.pesel : "Brak danych"}</div>
                <div>Adres {props.address ? props.address : "Brak danych"}</div>
                <div>Stanowisko {props.position ? props.position : "Brak danych"}</div>
                <hr></hr>
                Zadania

                { props.tasks !== null && (
                    props.tasks.map(task => (
                        (<div>{task.task}<label><button className="w-100 btn btn-lg btn-primary" onClick={() => {props.delete(task.id)}}>x</button></label></div>)
                    ))
                )}
                <hr></hr>
                Przypisz zadanie <input type="text" className="form-control" onChange={e => props.setTask(e.target.value)} />
                <hr></hr>
                <button className="w-100 btn btn-lg btn-primary" onClick={props.submit}>Dodaj zadanie</button>
                <button className="w-100 btn btn-lg btn-primary" onClick={props.onClose}>Zamknij</button>
            </div>
        </>
    )
};